/*!
 * mopane-builtins / Component
 * Licensed under the EUPL V.1.2
 */


export enum ComponentStatus {
  Constructed,
  Deinitialized,
  Deinitializing,
  Initialized,
  Initializing,
  Running
}

export interface Component {
  deinitialize(...args: any[]): Promise<void> | void;
  initialize(...args: any[]): Promise<void> | void;
}

export interface RunnableComponent extends Component {
  run(...args: any[]): Promise<any> | any;
}


export abstract class ComponentClass implements Component {
  status: ComponentStatus = ComponentStatus.Constructed;

  deinitialize() {
    if (this.status !== ComponentStatus.Initialized) {
      throw new Error(`Component '${this.toString()}' can't be deinitialized because it has status '${ComponentStatus[this.status]}'`);
    }

    this.status = ComponentStatus.Deinitializing;
  }

  initialize() {
    if (this.status !== ComponentStatus.Constructed) {
      throw new Error(`Component '${this.toString()}' can't be initialized because it has status '${ComponentStatus[this.status]}'`);
    }

    this.status = ComponentStatus.Initializing;
  }

  protected _doneDeinitializing() {
    if (this.status !== ComponentStatus.Deinitializing) {
      throw new Error('Illegal operation');
    }

    this.status = ComponentStatus.Deinitialized;
  }

  protected _doneInitializing() {
    if (this.status !== ComponentStatus.Initializing) {
      throw new Error('Illegal operation');
    }

    this.status = ComponentStatus.Initialized;
  }
}

export abstract class RunnableComponentClass extends ComponentClass implements RunnableComponent {
  run() {
    if (this.status !== ComponentStatus.Initialized) {
      throw new Error(`Component '${this.toString()}' can't be ran because it has status '${ComponentStatus[this.status]}'`);
    }

    this.status = ComponentStatus.Running;
  }

  protected _doneRunning() {
    if (this.status !== ComponentStatus.Running) {
      throw new Error('Illegal operation');
    }

    this.status = ComponentStatus.Initialized;
  }
}

/*!
 * mopane-fs / InputManager
 * Licensed under the EUPL V.1.2
 */


import * as fs from 'fs';
import { Component, DataStream, DriverInstances, TaskRunner } from 'mopane-core';

import { mergeFileEvent, mergeFileEvents } from './merge-events';
import { FSSchema } from './schemas/fs';
import { relativizePath } from './util';


/**
 * Each file is represented by a 'record', that stores cache data about its    |
 * properties (contents, mode, etc.) and information about its modification,
 * its collection and related descriptors.
 *
 * Records can be frozen to avoid instability across plugins and even in the
 * same plugin. For instance, if, for some reason, a plugin requires the same
 * file twice in a short time interval, the file contents might have changed
 * and cause a pipeline error.
 *
 * A descriptor is a number (similar to file descriptors) that represents a
 * pattern of files that a component of the pipeline is interested in (usually
 * a plugin). Each time that this component wants to get an update on files of
 * that pattern, it calls collectEvent(<descriptor>). This method returns an
 * array of events from the 'fs' schema.
 *
 * Many freezing behaviors are allowed when dealing with input managers:
 *   1) never freeze records (None)
 *   2) freeze all records that are linked to the requested descriptor (Related)
 *   3) freeze all records that are linked to a descriptor that has a record
 *        linked to the requested descriptor (Extended)
 *   4) freeze all records on the first collection (All)
 *
 *
 * The implemented solution is known as beta-configuration (ß-configuration)
 * because it creates a new watcher object (provided by the fs-watcher driver)
 * for each descriptor, as opposed to alpha-configuration, which uses a single
 * watcher for all file request patterns. The reason for this choice is mostly
 * the lack of functionality in Chokidar, used in the underneath driver.
 *
 */

export class InputManager implements Component {

  descriptors: Map<Descriptor, DescriptorData> = new Map();
  driver: DriverInstances['fs-watcher'] | undefined;
  records: Record[] = [];
  runner: TaskRunner;

  freezingStrategy: FreezingStrategy = FreezingStrategy.Related;
  nextDescriptor: Descriptor = 0;


  constructor(runner: TaskRunner) {
    this.runner = runner;
  }

  async initialize() {

  }

  collectEvents(descriptor: Descriptor): FSSchema.Event[] {
    let target = this.descriptors.get(descriptor);

    if (!target) {
      throw new Error(`Descriptor ${descriptor} does not exist`);
    }

    target.frozen = true;

    let events: FSSchema.Event[] = [];

    for (let record of this.records) {
      if (this.freezingStrategy === FreezingStrategy.All) {
        record.frozen = true;
      }

      if (record.descriptors.has(descriptor)) {
        if (this.freezingStrategy === FreezingStrategy.Related) {
          record.frozen = true;
        }

        events = [...events, ...record.events];
      }
    }

    return events;
  }

  endRun(aborted: boolean = false) {
    let runRequired = false;

    for (let record of this.records) {
      runRequired = runRequired || record.nextRunEvents.length > 0;

      record.frozen = false;
      record.events = aborted
        ? mergeFileEvents(record.events, record.nextRunEvents, record.filepath)
        : record.nextRunEvents;

      record.nextRunEvents = [];
    }

    // tslint:disable-next-line:no-unused-variable
    for (let [descriptor, data] of this.descriptors) {
      data.frozen = false;
    }

    if (!aborted) {
      this.records = this.records.filter((rec) => !rec.deleted || rec.events.length > 0);
    }

    if (runRequired) {
      this.runner.triggerRun();
    }
  }

  releaseDescriptor(descriptor: Descriptor) {
    if (!this.descriptors.has(descriptor)) {
      throw new Error(`Descriptor ${descriptor} does not exist`);
    }

    this.descriptors.delete(descriptor);
  }

  async requestFile(target: string): Promise<Descriptor> {
    if (!this.driver) {
      this.driver = await this.runner.profile.requestDriver('fs-watcher');
    }

    let descriptor = this.nextDescriptor++;

    this.descriptors.set(descriptor, {
      frozen: false,
      pattern: target
    });

    let watcher = (this.driver as DriverInstances['fs-watcher']).watch(target);
    let isFirstScan = true;

    watcher
      .on('add', (path, stats) => {

        // First, let's search for a record that would correspond to the added |
        // file. If that record is found, then one of the following is true:
        //
        //   1) The file was deleted and added again before its record was
        //      removed. We can check this with the 'deleted' flag on the
        //      record.
        //   2) The file was already being watched by another descriptor and
        //      whereas the current descriptor was just added and we're
        //      performing an initial scan. We can verify this using the
        //      'isFirstScan' variable.
        //   3) The file was just created, but another descriptor caught it
        //      first, in which case 'isFirstScan' would be false.
        //

        let record: Record | null = null;
        let isRecordNew = true;

        for (let testRecord of this.records) {
          if (testRecord.filepath === path) {
            record = testRecord;
            isRecordNew = false;

            break;
          }
        }

        // If, like most of the time, no record was present, we must create a
        // new one.
        if (!record) {
          record = {
            kind: RecordKind.File,
            filepath: path,
            relativeFilepath: relativizePath(path, this.runner.project.root),

            descriptors: new Set([descriptor]),
            events: [],
            nextRunEvents: [],

            deleted: false,
            frozen: false
          };

          this.records.push(record);
        }

        // For 2) & 3), we can make sure that the descriptor is attached to
        // the record. 'record.descriptors' being a set, there is no risk
        // of adding a duplicate as it would be the case with an array.
        record.descriptors.add(descriptor);

        // If the file is new, we must freeze it immediately if the freezing
        // stategy is set on 'Related' and the descriptor was frozen earlier on.
        record.frozen = record.frozen || (this.descriptors.get(descriptor) as DescriptorData).frozen
          && this.freezingStrategy === FreezingStrategy.Related;

        // There is no need to add an event if another watcher already did that.
        if (!isRecordNew && !record.deleted) {
          return;
        }

        // To address 1), we simply mark the record non-deleted if it wasn't.
        // The 'fs.delete' and 'fs.create' events will be merged below.
        record.deleted = false;

        let event: FSSchema.FileCreationEvent = {
          type: 'fs.create',
          filepath: record.relativeFilepath,

          contents: DataStream.fromStream(fs.createReadStream(path)),
          mode: (stats && stats.mode) || null,

          toString() {
            return `[fs] create ${this.filepath}`;
          }
        };

        this.addEventToRecord(event, record);

        if (!isFirstScan && !record.frozen) {
          this.runner.triggerRun();
        }
      });

    watcher
      .on('change', (path, stats) => {
        let record = this.records.find((rec) => rec.filepath === path);

        if (!record) {
          throw new Error('wtf');
        }

        let event: FSSchema.FileUpdateEvent = {
          type: 'fs.update',
          filepath: record.relativeFilepath,

          contents: DataStream.fromStream(fs.createReadStream(path)),
          mode: (stats && stats.mode) || null,

          toString() {
            return `[fs] update ${this.filepath}`;
          }
        };

        this.addEventToRecord(event, record);

        if (!record.frozen) {
          this.runner.triggerRun();
        }
      });

    watcher.on('delete', (path) => {
      let record = this.records.find((rec) => rec.filepath === path);

      if (!record) {
        throw new Error('wtf');
      }

      let event: FSSchema.FileDeletionEvent = {
        type: 'fs.delete',
        filepath: record.relativeFilepath,

        toString() {
          return `[fs] delete ${this.filepath}`;
        }
      };

      record.deleted = true;

      this.addEventToRecord(event, record);

      if (!record.frozen) {
        this.runner.triggerRun();
      }
    });

    await new Promise((resolve) => {
      watcher
        .on('ready', () => {
          isFirstScan = false;

          resolve();
        });
    });

    return descriptor;
  }

  async deinitialize() {

  }


  private addEventToRecord(event: FSSchema.Event, record: Record) {
    if (record.frozen) {
      record.nextRunEvents = mergeFileEvent(record.nextRunEvents, event, record.relativeFilepath);
    } else {
      record.events = mergeFileEvent(record.events, event, record.relativeFilepath);
    }
  }
}


export type Descriptor = number;

export interface DescriptorData {
  frozen: boolean;
  pattern: string;
}


export type Record = FileRecord;

export enum RecordKind {
  File
}

export interface FileRecord {
  kind: RecordKind;
  filepath: string;
  relativeFilepath: string;

  descriptors: Set<Descriptor>;
  events: FSSchema.Event[];
  nextRunEvents: FSSchema.Event[];

  deleted: boolean;
  frozen: boolean;
}


export enum FreezingStrategy {
  All, None, Related
}


/*!
 * mopane-fs / utilities
 * Licensed under the EUPL V.1.2
 */


import * as filesystem from 'fs';
import * as path from 'path';
import { Readable as ReadableStream } from 'stream';
import { promisify } from 'util';


export function absolutizePath(target: string, root: string) {
  return !path.isAbsolute(target)
    ? path.join(root, target)
    : target;
}

export function relativizePath(target: string, root: string) {
  let relativePath = path.relative(root, target);

  return !relativePath.startsWith('..')
    ? relativePath
    : target;
}


export namespace fsutil {
  export async function mkdirp(fs: typeof filesystem, dir: string) {
    let mkdir = promisify(fs.mkdir);
    let stat = promisify(fs.stat);

    // node v10.12.0+
    /* try {
      await mkdir(dir, { recursive: true });
    } catch (err) {
      if (err.code && err.code === 'EEXIST') return;

      throw err;
    } */

    // other versions
    try {
      await mkdir(dir);
    } catch (err) {
      if (!err.code) throw err;

      switch (err.code) {
        case 'EEXIST':
          let stats = await stat(dir);

          if (!stats.isDirectory()) throw err;
          return;

        case 'ENOENT':
          await mkdirp(fs, path.dirname(dir))
          await mkdirp(fs, dir);

          break;

        default:
          throw err;
      }
    }
  }

  export function writeFromStream(fs: typeof filesystem, fd: number, inputStream: ReadableStream) {
    return new Promise((resolve, reject) => {
      let stream = fs.createWriteStream('', { fd });

      inputStream.pipe(stream);

      stream.on('error', (err: Error) => {
        reject(err);
      });

      stream.on('finish', () => {
        resolve();
      });
    });
  }
}


/*!
 * mopane-builtins / empty plugin
 * Licensed under the EUPL V.1.2
 */


import { Interval, Plugin } from 'mopane-core';


export function empty(channels?: Interval): Plugin {
  return function (emit) {
    return (event) => {
      if (event.type === 'end') {
        emit(event);
      }
    };
  };
}

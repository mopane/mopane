/*!
 * mopane-builtins / write plugin
 * Licensed under the EUPL V.1.2
 */


import * as path from 'path';
import { FocusedPlugin, Interval, Plugin, PluginContext } from 'mopane-core';
import { FSSchema, absolutizePath } from 'mopane-fs';


export function write(options?: Options): FocusedPlugin<FSSchema.Event>;
export function write(prefix: string, options?: Options): FocusedPlugin<FSSchema.Event>;
export function write(arg1: Options | string = '', arg2?: Options): FocusedPlugin<FSSchema.Event> {
  let prefix = typeof arg1 === 'string' ? arg1 : '';
  let options = typeof arg1 === 'object' ? arg1 : (arg2 || {});

  let context: PluginContext | null = null;
  let events: FSSchema.Event[] = [];

  let consumeEvent = (event: FSSchema.Event) => {
    return (context as PluginContext).runner.fsOutputInterface.consume(event, {
      disableOverride: options.disableOverride,
      disableStructure: options.disableStructure
    });
  };

  return {
    initialize(ctx) {
      context = ctx;
    },

    run(emit) {
      let deferred: Promise<void>[] = [];
      let root = (context as PluginContext).project.root;

      events = [];

      return (event) => {
        if (
          FSSchema.isEvent(event)
          && (!options.channels || event.channel !== void 0 && options.channels.matchesValue(event.channel))) {
          let finalEvent = {
            ...event,
            filepath: absolutizePath((event as FSSchema.Event).filepath, path.join(root, prefix))
          } as FSSchema.Event;

          if (event.type !== 'fs.delete' || !options.ignoreSubstractiveEvents) {
            events.push(finalEvent);

            if (!options.consumeOnCommit) {
              deferred.push(consumeEvent(finalEvent));
            }
          }

          if (!options.preserveEvents) {
            return;
          }
        }

        if (event.type !== 'end') {
          emit(event);
          return;
        }

        Promise.all(deferred)
          .catch((err: Error) => {
            emit({ type: 'error', error: err });
          })
          .then(() => {
            emit(event);
          });
      };
    },


    async commit() {
      if (options.consumeOnCommit) {
        await Promise.all(
          events.map((e) => consumeEvent(e))
        );
      }
    }
  };
}

/**
 * OPTIONS
 *
 * Control when to write and clear targets
 *   - consumeOnCommit: only proceed when commit() is called; events will be
 *       consumed in the same order as they were provided (no merging will
 *       happen)
 *   - clearOnDiscard [not implemented]: remove produced files when discard()
 *       is called (only effective when 'writeOnCommit' isn't set)
 *   - clearOnExit [not implemented]: remove produced files when deinitializing
 * 
 * Control what files to expect, allow and override
 *   - disableOverride: fail if targets exist
 *   - disableStructure: do not create missing directories
 *   - overrideTargets [not implemented]: if needed, override a file if it
 *       isn't of the type of a target of the same name
 * 
 * Misc
 *   - channels: interval of channels containing events to consume
 *       (default: {0})
 *   - ignoreSubstractiveEvents: ignore fs.delete events (they will still be
 *       consumed unless 'preserveEvents' is set) unless they cause issues
 *       later on
 *   - preserveEvents: re-emit consumed events
 *
 */
interface Options {
  channels?: Interval;
  clearOnDiscard?: boolean;
  clearOnExit?: boolean;
  consumeOnCommit?: boolean;
  disableOverride?: boolean;
  disableStructure?: boolean;
  ignoreSubstractiveEvents?: boolean;
  overrideTargets?: boolean;
  preserveEvents?: boolean;
}


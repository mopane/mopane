/*!
 * mopane-core / root schema
 * Licensed under the EUPL V.1.2
 */


import { PipelineEvent } from '../plugin';
import { SchemaGuardInstance } from '../schema-guard';


export namespace RootSchema {
  export type Event = CloseEvent | EndEvent | ErrorEvent;

  export interface CloseEvent extends PipelineEvent {
    type: 'close';
  }

  export interface EndEvent extends PipelineEvent {
    type: 'end';
  }

  export interface ErrorEvent extends PipelineEvent {
    type: 'error';
    error: Error;
  }


  export function guard(): SchemaGuardInstance {
    let ended = false;

    return (event) => {
      if (ended) {
        throw new Error(`Unexpected event '${event}', pipeline is already ended`);
      }

      if (event.type === 'end') {
        ended = true;
      }
    };
  }

  export function isEvent(event: PipelineEvent): event is Event {
    return (['close', 'end', 'error'] as (string | symbol)[]).includes(event.type);
  }
}


/*!
 * mopane-core / utilities
 * Licensed under the EUPL V.1.2
 */


export type PossiblyDeferred<T> = Promise<T> | T;
export type Deferred<T> = T extends Promise<any> ? T : Promise<T>;


export function arrReverse<T>(arr: T[]): T[] {
  return Array.from(arr).reverse();
}

export async function runDeferredChain<T>(initialValue: T, tasks: ((value: T) => PossiblyDeferred<T> | PossiblyDeferred<void>)[]): Promise<T> {
  return await tasks
    .reduce(async (deferredValue: PossiblyDeferred<T>, task) => {
      let value = await deferredValue;
      return (await task(value)) || value;
    }, initialValue);
}


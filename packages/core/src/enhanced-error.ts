/*!
 * mopane-core / EnhancedError
 * Licensed under the EUPL V.1.2
 */


const Style = {
  Cyan: '\u001b[36m',
  Blue: '\u001b[34m',
  Red: '\u001b[31m',
  White: '\u001b[37m',

  BackgroundRed: '\u001b[41m',

  Reset: '\u001b[0m'
};

const LanguageKeywords: {
  [lang: string]: RegExp
} = {
  javascript: /function|return|var/g
};

const defaultWidth = Math.max(Math.min(process.stdout.columns || 80, 120), 80);


class DefaultError extends Error {}

export namespace EnhancedError {

  export class Error implements DefaultError {
    kind: string;
    location: string | void;
    nodes: Node[];

    message: string;
    name: string;
    stack: string | undefined;

    constructor(options: Options, base?: DefaultError) {
      this.kind = options.kind;
      this.location = options.location;
      this.message = options.message;
      this.name = base ? base.name : 'EnhancedError';
      this.stack = base ? base.stack : new DefaultError(this.message).stack;

      this.nodes = options.description.map((item) => {
        if (Array.isArray(item)) {
          return { type: NodeType.List, items: item } as ListNode;
        } if (typeof item === 'string') {
          return { type: NodeType.Text, value: item } as TextNode;
        }

        return item;
      });
    }

    display(width: number = defaultWidth): string {

      // formatting methods

      let ctx = {
        assemble(words: string[], options: { indentation?: number; margins?: number } = {}) {
          let indentation = options.indentation || 0;
          let margins = options.margins || 0;


          let outLines: string[][] = [];

          let lineLength = -1;
          let currentLine: string[] = [];

          words.forEach((word, wordIndex) => {
            lineLength += word.length + 1;

            if (lineLength > width - indentation - margins * 2) {
              outLines.push(currentLine);
              
              lineLength = word.length;
              currentLine = [];
            }

            currentLine.push(word);
          });

          outLines.push(currentLine);

          return outLines.map((line) => ' '.repeat(indentation + margins) + line.join(' '));
        },

        style(modifier: string, target: string) {
          if (true) {
            return modifier + target + Style.Reset;
          }

          return target;
        },

        text(str: string) {
          let occurence;
          let regexp = /(\*[^\*]*\*)/g;

          let out = '';
          let previousEndIndex = 0;
          
          while ((occurence = regexp.exec(str)) !== null) {
            out += str.substring(previousEndIndex, occurence.index)
              + ctx.style(Style.White, str.substring(occurence.index + 1, regexp.lastIndex - 1));

            previousEndIndex = regexp.lastIndex;
          }

          out += str.substring(previousEndIndex);

          return out;
        }
      };
      
      let output: string[] = [];


      // top bar

      let leftPart = `-- ${this.kind.toUpperCase()} `;
      let rightPart = this.location ? ` ${this.location}` : '';
      let middlePart = '-'.repeat(width - leftPart.length - rightPart.length);

      output.push(ctx.style(Style.Cyan, leftPart + middlePart + rightPart));
      output.push('');


      // contents

      for (let node of this.nodes) {
        switch (node.type) {
          case NodeType.ErrorStack:
            output = [
              ...output,
              ...node.stack.split('\n')
            ];

            break;

          case NodeType.List:
            let indexWidth = Math.floor(Math.log10(node.items.length)) + 1;
            node.items.forEach((item, index) => {
              let lines = ctx.assemble(ctx.text(item).split(' '), { indentation: indexWidth + 6 });

              output = [
                ...output,
                '  ' + (index + 1).toString().padStart(indexWidth) + '. ' + lines[0].substring(indexWidth + 6),
                ...lines.slice(1)
              ];
            });

            break;

          case NodeType.Quote:
            output = [...output, ...ctx.assemble(node.value.split(' '), { margins: 2 })];
            break;

          case NodeType.Text:
            output = [...output, ...ctx.assemble(ctx.text(node.value).split(' '))];
            break;

          case NodeType.SourceCode:
            let lineIndexWidth = Math.max(...node.value.map((line) => line.index.toString().length));
            let keywords = node.language ? LanguageKeywords[node.language] || null : null;

            let previousIndex = -1;

            for (let line of node.value) {
              if (previousIndex >= 0 && previousIndex != line.index - 1) {
                output.push(`${'...'.padStart(lineIndexWidth + 2)} |`);
              }

              let value = line.value;

              if (keywords) {
                let occurence;

                while ((occurence = keywords.exec(line.value)) !== null) {
                  let a = occurence.index;
                  let b = keywords.lastIndex;
                  value = value.substring(0, a) + ctx.style(Style.Blue, value.substring(a, b)) + value.substring(b);
                }
              }

              output.push(`  ${line.index.toString().padStart(lineIndexWidth)} | ${value}`);

              if (line.underlining) {
                output.push(' '.repeat(lineIndexWidth + line.underlining[0] + 5) + ctx.style(Style.Red, '^'.repeat(line.underlining[1] - line.underlining[0])));
              }

              previousIndex = line.index;
            }

            break;

          default:
            output.push('<unknown information>');
        }

        output.push('');
      }

      return output.join('\n');
    }

    toString() {
      return `${this.name}: ${this.message}`;
    }
  }

  export interface Options {
    kind: string;
    location?: string;
    message: string;
    stack?: string;

    description: (Node | string | string[])[];
  }

  export enum NodeType {
    ErrorStack, List, Quote, Text, SourceCode
  }


  type Node = ErrorStackNode | ListNode | QuoteNode | TextNode | SourceCodeNode;

  interface ErrorStackNode {
    type: NodeType.ErrorStack;
    stack: string;
  }

  interface ListNode {
    type: NodeType.List;
    items: string[];
  }

  interface QuoteNode {
    type: NodeType.Quote;
    value: string;
  }

  interface TextNode {
    type: NodeType.Text;
    value: string;
  }

  interface SourceCodeNode {
    type: NodeType.SourceCode;
    language?: string;
    value: {
      index: number;
      value: string;
      underlining?: [number, number];
    }[];
  }
}


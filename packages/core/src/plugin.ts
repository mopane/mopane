/*!
 * mopane-core / Plugin
 * Licensed under the EUPL V.1.2
 */


import { RunnableComponent } from './component';
import { Processor } from './processor';
import { Project } from './project';
import { RootSchema } from './schemas/root';
import { TaskRunner } from './task-runner';
import { PossiblyDeferred } from './util';


export type Plugin = FunctionPluginHandler | PartialPlugin | StrictPlugin;
export type PluginInput = Plugin | FocusedPlugin<PipelineEventUnknown>;
export type Plugins = Iterable<Plugin>;

export interface PluginContext {
  processor: Processor;
  project: Project;
  runner: TaskRunner;
}


/* Emitter/listener functions */

// The PluginEmitterFunction type corresponds to the type
// of the 'emit' function given to plugins. It the plugin
// runs the function with 'null' as the event argument,
// the plugin will be considered to be a 'witness plugin'.
// All afterwards calls to the same function will be
// ignored.
//
// TODO: implement this feature by replacing
// 'PipelineEvent' with 'PipelineEvent | null'.
export type PluginEmitterFunction = <T extends PipelineEvent>(event: T) => void;

// The PluginListenerFunction corresponds to the type of
// the function returned by a plugin.
export type PluginListenerFunction = (event: PipelineEvent) => void;

// A plugin can also return 'null' to indicate that it
// does not wish to listen to events (all events will be
// forwarded), hence the union type with
// PluginListenerFunction.
export type PluginHandler = (emit: PluginEmitterFunction) => PluginListenerFunction | null;


/* Plugin objects */

export interface PluginHelperMethods {
  initialize(ctx: PluginContext): PossiblyDeferred<void>;
  deinitialize(): PossiblyDeferred<void>;

  commit(): PossiblyDeferred<void>;
  discard(): PossiblyDeferred<void>;
}

export interface StrictPlugin extends PluginHelperMethods {
  run: PluginHandler;
}

export interface PartialPlugin extends Partial<PluginHelperMethods> {
  run: PluginHandler;
}

export function ensureStrictPlugin(plugin: PluginInput): StrictPlugin {
  if (typeof plugin === 'function') {
    return ensureStrictPlugin(new FunctionPlugin(plugin));
  }

  return {
    commit: (plugin.commit || (() => {})).bind(plugin),
    discard: (plugin.discard || (() => {})).bind(plugin),
    initialize: (plugin.initialize || (() => {})).bind(plugin),
    run: plugin.run.bind(plugin),
    deinitialize: (plugin.deinitialize || (() => {})).bind(plugin)
  };
}


/* Function plugins */

export type FunctionPluginHandler = (this: PluginContext, emit: PluginEmitterFunction) => PluginListenerFunction | null;

export class FunctionPlugin implements PartialPlugin {
  private context: PluginContext | undefined;
  private handler: FunctionPluginHandler;

  constructor(handler: FunctionPluginHandler) {
    this.handler = handler;
  }

  initialize(context: PluginContext) {
    this.context = context;
  }

  run(emit: PluginEmitterFunction) {
    return this.handler.call(this.context as PluginContext, emit);
  }
}


/* Focused plugins */

// A 'focused plugin' is a plugin which can, in theory, only receive and emit
// certain types of events. This is useful for the type system to check if
// events are used correctly as type guards are quite efficient white objects
// carrying a string literal property as opposed to a normal string.

// These events will be included no matter what the plugin declares it
// uses/receives.
export type FocusedPluginRootEvents = RootSchema.Event | PipelineEventUnidentified;

export interface FocusedPlugin<Received extends PipelineEvent, Emitted extends PipelineEvent = FocusedPluginReceivedEvent<Received>> extends Partial<PluginHelperMethods> {
  run(emit: FocusedPluginEmitterFunction<Emitted>): (event: FocusedPluginReceivedEvent<Received>) => void;
}

export type FocusedPluginEmitterFunction<Emitted> = (event: Emitted | FocusedPluginRootEvents) => void;
export type FocusedPluginReceivedEvent<Received> = FocusedPluginRootEvents | Received;

export type FocusedPluginHandler<Received extends PipelineEvent, Emitted extends PipelineEvent = FocusedPluginReceivedEvent<Received>> = (emit: FocusedPluginEmitterFunction<Emitted>) => (event: FocusedPluginReceivedEvent<Received>) => void;


/* Events */

export interface PipelineEvent {
  type: string | symbol;
  channel?: number | void;
  toString?(): string;
}

export interface PipelineEventUnknown extends PipelineEvent {
  [key: string]: any;
}

export interface PipelineEventUnidentified extends PipelineEvent {
  type: symbol;
}


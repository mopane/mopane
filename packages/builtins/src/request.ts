/*!
 * mopane-builtins / request plugin
 * Licensed under the EUPL V.1.2
 */


import * as http from 'http';
import { DataStream, FocusedPlugin, Interval, PipelineEvent, RootSchema } from 'mopane-core';
import { FSSchema } from 'mopane-fs';


type ReturnedPlugin = FocusedPlugin<RequestSchema.RequestEvent | FSSchema.FileCreationEvent>;

export function request(input: string, output: string, options?: GlobalOptions): ReturnedPlugin;
export function request(targets: Targets, options?: GlobalOptions): ReturnedPlugin;
export function request(param1: Targets | string, param2?: GlobalOptions | string, param3?: GlobalOptions): ReturnedPlugin {
  let targets: Targets;
  let options: GlobalOptions;

  if (typeof param1 === 'object') {
    targets = param1;
    options = (param2 || {}) as GlobalOptions;
  } else {
    targets = { [param1]: param2 as string };
    options = param3 || {};
  }

  return {
    run(emit) {
      let requested = 0;
      let done = 0;
      let endEvent: RootSchema.EndEvent | null = null;

      let requestTarget = (target: string, dest: string, opts: LocalOptions = options) => {
        requested++;

        let req = http.get(target, /* opts.request, */ (res) => {
          if (res.statusCode !== 200) {
            emit({ type: 'error', error: new Error(`Invalid HTTP code ${res.statusCode}`) });
          } else {
            emit({
              channel: opts.channel || 0,
              type: 'fs.create',

              filepath: dest,
              contents: DataStream.fromStream(res),

              toString: () => `[fs] create ${dest}`
            });
          }

          res.on('data', (chunk) => {});

          res.on('end', () => {
            done++;
            checkIfDone();
          });
        });

        req.on('error', (error) => {
          emit({ type: 'error', error });

          done++;
          checkIfDone();
        });
      };

      let checkIfDone = () => {
        if (endEvent && done >= requested) {
          emit(endEvent);
        }
      };

      for (let target in targets) {
        requestTarget(target, targets[target]);
      }

      return (event) => {
        switch (event.type) {
          case 'end':
            endEvent = event;
            checkIfDone();

            break;

          case 'request': {
            if (options.inputEventsChannels && (
              event.channel === void 0
              || !options.inputEventsChannels.matchesValue(event.channel)
            )) break;

            requestTarget(event.target, event.destination, {
              channel: event.channel || void 0,
              ...options,
              ...(event.options || {})
            });

            break;
          }

          default:
            emit(event);
        }
      };
    }
  };
}

interface GlobalOptions {
  channel?: number;
  inputEventsChannels?: Interval;
  request?: http.RequestOptions;
}

interface LocalOptions {
  channel?: number;
  request?: http.RequestOptions;
}

interface Targets {
  [input: string]: string;
}

export namespace RequestSchema {
  export interface RequestEvent extends PipelineEvent {
    type: 'request';

    destination: string;
    options?: LocalOptions;
    target: string;
  }
}


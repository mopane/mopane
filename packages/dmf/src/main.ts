/*!
 * mopane-dmf / main
 * Licensed under the EUPL V.1.2
 */


import * as fs from 'fs';
import * as path from 'path';
import { DataStream, FocusedPluginEmitterFunction, FocusedPluginReceivedEvent, FocusedPluginHandler } from 'mopane-core';
import { FSSchema } from 'mopane-fs';


export function createInputFS() {
  let files: File[] = [];
  let descriptors = new Map<number, File>();
  let descriptorIndex = 0;

  let ended = false;
  let deferredEndResolve: () => void;
  let deferredEnd: Promise<void> = new Promise((resolve) => {
    deferredEndResolve = resolve;
  });

  deferredEnd
    .then(() => {
      ended = true;
    });


  let findFile = (filepath: string, channel: number | void): File | undefined =>
    files.find((f) => f.filepath === filepath && (channel === void 0 || f.channel === channel));

  let receive = (event: FocusedPluginReceivedEvent<FSSchema.Event>): boolean => {
    switch (event.type) {
      case 'fs.create':
        files.push({
          channel: event.channel !== void 0 ? event.channel : void 0,
          contents: event.contents.getBuffer(),
          filepath: event.filepath
        });

        return true;

      case 'fs.update':
        let file = findFile(event.filepath, event.channel);
        file!.contents = event.contents.getBuffer();

        return true;

      case 'end':
        deferredEndResolve();
    }

    return false;
  };

  let fs = {
    close(fd: number, callback: (err: Error | undefined) => void) {
      if (!descriptors.has(fd)) {
        callback(new FSError(FSErrorType.ENXIO, 'close', fd.toString()));
      } else {
        descriptors.delete(fd);
        callback(void 0);
      }
    },

    open(filepath: string, callback: (err: Error | undefined, fd?: number) => void) {
      let [resolvedFilepath, channel] = deserializePath(filepath);

      deferredEnd!
        .then(() => {
          let file = findFile(resolvedFilepath);

          if (!file) {
            throw new FSError(FSErrorType.ENOENT, 'open', filepath);
          }

          let fd = descriptorIndex++;

          descriptors.set(fd, file);
          callback(void 0, fd);
        })
        .catch((err) => {
          callback(err);
        });
    },

    readFile(filepath: number | string, callback: (err: Error | undefined, data?: Buffer | string) => void) {
      deferredEnd!
        .then(() => {
          let file: File;

          if (typeof filepath === 'number') {
            if (!descriptors.has(filepath)) {
              throw new FSError(FSErrorType.ENXIO, 'read', filepath.toString());
            }

            file = descriptors.get(filepath)!;
          } else {
            let f = findFile(deserializePath(filepath)[0]);

            if (!f) {
              throw new FSError(FSErrorType.ENOENT, 'open', filepath);
            }

            file = f;
          }

          return file.contents;
        })
        .then((data) => {
          callback(void 0, data);
        })
        .catch((err) => {
          callback(err);
        });
    },

    readdir(filepath: string, callback: (err: Error | undefined, files?: string[]) => void) {
      deferredEnd!
        .then(() => {
          let [resolvedFilepath, channel] = deserializePath(filepath);

          callback(void 0,
            files
              .filter((file) => {
                return path.dirname(file.filepath) === resolvedFilepath && file.channel === channel;
              })
              .map((file) => {
                return serializePath(file.filepath, file.channel);
              })
          );
        })
        .catch((err) => {
          callback(err);
        });
    },

    readlink(filepath: string, callback: (err: Error | undefined, value?: string) => void) {
      callback(new FSError(FSErrorType.EINVAL, 'readlink', filepath));
    },

    stat(filepath: string, callback: (err: Error | undefined, stats?: Partial<fs.Stats>) => void) {
      deferredEnd!
        .then(() => {
          let [deserializedFilepath, channel] = deserializePath(filepath);

          let file = findFile(deserializedFilepath, channel);
          let dir: string | undefined;

          let common = {};

          if (file) {
            file.contents
              .then((data) => {
                callback(void 0, {
                  ...common,
                  isDirectory: () => false,
                  isFile: () => true,
                  size: data.length
                });
              })
              .catch((err) => {
                callback(err);
              });
          } else {
            for (let file of files) {
              if (file.channel !== channel) {
                continue;
              }

              let segments = file.filepath.split('/');

              for (let index = 0; index < segments.length - 1; index++) {
                let dirPath = segments.slice(0, index).join('/');

                if (dirPath === deserializedFilepath) {
                  dir = dirPath;
                  break;
                }
              }

              if (dir) {
                break;
              }
            }

            if (dir) {
              callback(void 0, {
                ...common,
                isDirectory: () => true,
                isFile: () => false
              });
            } else {
              throw new FSError(FSErrorType.ENOENT, 'stat', filepath);
            }
          }
        })
        .catch((err) => {
          callback(err);
        });
    }
  };

  return { fs, receive };
}


export function createOutputFS(optionalEmit?: FocusedPluginEmitterFunction<FSSchema.Event>) {
  let emit: FocusedPluginEmitterFunction<FSSchema.Event> | null = optionalEmit || null;

  let pause = () => {
    emit = null;
  };

  let resume = (_emit: FocusedPluginEmitterFunction<FSSchema.Event>) => {
    emit = _emit;
  };

  let absolutizePath = (filepath: string): string => {
    return path.isAbsolute(filepath)
      ? filepath
      : path.join('/project', filepath);
  };

  let existingPaths = new Set<string>();

  let fs = {
    join(...filepaths: string[]) {
      return path.join(...filepaths);
    },

    mkdir(filepath: string, callback: (err: Error | undefined) => void) {
      try {
        fs.mkdirSync(filepath);
        callback(void 0);
      } catch (err) {
        callback(err);
      }
    },

    mkdirSync(filepath: string) {

    },

    mkdirp(filepath: string, callback: (err: Error | undefined) => void) {
      fs.mkdir(filepath, callback);
    },

    rmdir(filepath: string, callback: (err: Error | undefined) => void) {
      try {
        fs.rmdirSync(filepath);
        callback(void 0);
      } catch (err) {
        callback(err);
      }
    },

    rmdirSync(filepath: string) {

    },

    unlink(filepath: string, callback: (err: Error | undefined) => void) {
      try {
        fs.unlinkSync(filepath);
        callback(void 0);
      } catch (err) {
        callback(err);
      }
    },

    unlinkSync(filepath: string) {
      if (!emit) {
        throw new FSError(FSErrorType.EPERM, 'unlink', filepath);
      }

      let absFilepath = absolutizePath(filepath);
      let [resolvedFilepath, channel] = deserializePath(absFilepath);

      emit({
        type: 'fs.delete',
        channel,

        filepath: resolvedFilepath
      });

      existingPaths.delete(absFilepath);
    },

    writeFile(filepath: string, data: Buffer, callback: (err: Error | undefined) => void) {
      try {
        fs.writeFileSync(filepath, data);
        callback(void 0);
      } catch (err) {
        callback(err);
      }
    },

    writeFileSync(filepath: string, data: Buffer) {
      if (!emit) {
        throw new FSError(FSErrorType.EPERM, 'write', filepath);
      }

      let absFilepath = absolutizePath(filepath);
      let [resolvedFilepath, channel] = deserializePath(absFilepath);

      emit({
        type: existingPaths.has(resolvedFilepath) ? 'fs.update' : 'fs.create',
        channel,

        filepath: resolvedFilepath,
        contents: DataStream.fromBuffer(data),
      } as FSSchema.Event);

      existingPaths.add(absFilepath);
    }
  };

  return { fs, pause, resume };
}


function serializePath(filepath: string, channel: number | void): string {
  return path.join('/' + (channel !== void 0 ? channel : 'project'), filepath);
}

function deserializePath(filepath: string): [string, number] {
  let volumeName = filepath.split('/')[1];
  let channel = parseInt(volumeName);

  if (channel !== channel) {
    channel = 0;
  }

  let resolvedFilepath = path.relative('/' + volumeName, filepath);

  return [resolvedFilepath, channel];
}


class FSError extends Error {
  readonly code: string;
  readonly path: string;
  readonly syscall: string;

  constructor(type: FSErrorType, func: string, filepath: string) {
    super((() => {
      switch (type) {
        case FSErrorType.EINVAL:
          return `EINVAL: invalid argument, ${func} ${filepath}`;

        case FSErrorType.ENOENT:
          return `ENOENT: no such file or directory, ${func} ${filepath}`;

        case FSErrorType.ENXIO:
          return `ENXIO: no such device or address, ${func} ${filepath}`;

        case FSErrorType.EPERM:
          return `EPERM: operation not permitted, ${func} ${filepath}`;

        default:
          return `${FSErrorName[type]}: operation failed, ${func} ${filepath}`;
      }
    })());

    this.code = FSErrorName[type];
    this.path = filepath;
    this.syscall = func;
  }
}


export interface File {
  channel: number | undefined;
  contents: Promise<Buffer>;
  filepath: string;
}

enum FSErrorType {
  EINVAL, ENOENT, ENXIO, EPERM
}

enum FSErrorName {
  EINVAL = 'EINVAL',
  ENOENT = 'ENOENT',
  ENXIO = 'ENXIO',
  EPERM = 'EPERM'
}


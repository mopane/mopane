/*!
 * mopane-core / channel schema
 * Licensed under the EUPL V.1.2
 */


import { Interval } from '../interval';
import { PipelineEvent } from '../plugin';
import { SchemaGuardInstance } from '../schema-guard';


export namespace ChannelSchema {
  export type Event = ActiveEvent | CloseEvent;

  export interface ActiveEvent extends PipelineEvent {
    type: 'ch.active';

    channels: Interval;
  }

  export interface CloseEvent extends PipelineEvent {
    type: 'ch.close';

    channel: number;
  }

  /* export interface ClassifiedEvent extends PipelineEvent {
    channel: number;
  } */


  export function guard(): SchemaGuardInstance {
    let activeChannels: Interval;
    let closedChannels = new Set<number>();
    let observedChannels = new Set<number>();

    return (event) => {
      if (event.channel !== void 0) {
        if (activeChannels && !activeChannels.matchesValue(event.channel)) {
          throw new Error(`Unexpected event '${event}' in channel supposedly non-active ${event.channel}`);
        }

        if (closedChannels.has(event.channel)) {
          throw new Error(`Unexpected event '${event}' in closed channel ${event.channel}`);
        }

        observedChannels.add(event.channel);
      }

      switch (event.type) {
        case 'ch.active':
          if (activeChannels) {
            throw new Error(`Unexpected 'ch.active' event, active channels are already known`);
          }

          activeChannels = (event as ActiveEvent).channels;

          for (let channel of observedChannels) {
            if (!activeChannels.matchesValue(channel)) {
              throw new Error(`Unexpected inactivity of channel ${channel}, which already received an event`);
            }
          }

          break;

        case 'ch.close':
          let e = event as CloseEvent;

          if (closedChannels.has(e.channel)) {
            throw new Error(`Unexpected 'ch.close' event on already-closed channel ${event.channel}`);
          }

          closedChannels.add(e.channel);
          break;
      }
    };
  }

  export function isEvent(event: PipelineEvent): event is Event {
    return typeof event.type === 'string' && event.type.startsWith('ch.');
  }
}


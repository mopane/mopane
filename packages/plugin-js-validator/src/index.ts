/*!
 * mopane-plugin-js-validator
 * Licensed under the EUPL V.1.2
 */


export const version = '0.0.0';

export * from './plugin';


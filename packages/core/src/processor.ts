/*!
 * mopane-core / Processor
 * Licensed under the EUPL V.1.2
 */


import { Component, RunnableComponent } from './component';
import { PipelineEvent, PluginEmitterFunction, PluginListenerFunction, PluginContext, PluginHandler, StrictPlugin, Plugins, ensureStrictPlugin, } from './plugin';
import { TaskRunner } from './task-runner';
import { arrReverse } from './util';


export class Processor implements RunnableComponent {
  plugins: StrictPlugin[] = [];
  runner: TaskRunner;

  layers: Partial<ProcessorLayer>[] = [];
  layersCascading: {
    layerSource: ProcessorLayerSource;
    options: Partial<ProcessorLayerOptions>;
  }[] = [];
  layerMethods: {
    deinitialize: ProcessorLayer['deinitialize'][],
    initialize: ProcessorLayer['initialize'][],
    postPluginRun: ProcessorLayer['postPluginRun'][],
    postRun: ProcessorLayer['postRun'][]
    prePluginRun: ProcessorLayer['prePluginRun'][],
    preRun: ProcessorLayer['preRun'][],
    subprocess: ProcessorLayer['subprocess'][]
  } = {
    deinitialize: [],
    initialize: [],
    postPluginRun: [],
    postRun: [],
    prePluginRun: [],
    preRun: [],
    subprocess: []
  };

  running: boolean = false;

  constructor(plugins: Plugins, options: ProcessorOptions) {
    for (let plugin of plugins) {
      this.plugins.push(ensureStrictPlugin(plugin));
    }

    this.runner = options.runner;
  }

  async commit() {
    await Promise.all(
      this.plugins.map((plugin) => plugin.commit())
    );
  }

  async deinitialize() {
    await Promise.all([
      ...this.layerMethods.deinitialize.map((method) => method()),
      ...this.plugins.map((plugin) => plugin.deinitialize())
    ]);
  }

  async discard() {
    await Promise.all(
      this.plugins.map((plugin) => plugin.discard())
    );
  }

  async initialize() {
    let context: PluginContext = {
      processor: this,
      project: this.runner.project,
      runner: this.runner
    };

    await Promise.all([
      ...this.layerMethods.initialize.map((method) => method()),
      ...this.plugins.map((plugin) => plugin.initialize(context))
    ]);
  }

  run(rootEmit: PluginEmitterFunction) {
    this.running = true;

    let recordedEndEvent: PipelineEvent | null = null;

    let emitter: PluginListenerFunction = ((emit) => (event: PipelineEvent) => {
      if (event.type === 'end') {
        if (!recordedEndEvent || event !== recordedEndEvent) {
          emit({ type: 'error', error: new Error(`Output 'end' event does not match recorded original`) });
        }

        this.running = false;
      }
      
      emit(event);
    })(rootEmit);

    for (let method of arrReverse(this.layerMethods.postRun)) {
      emitter = method(emitter) || emitter;
    }

    let reversedPlugins = arrReverse(this.plugins);

    for (let index = 0; index < reversedPlugins.length; index++) {
      let plugin = reversedPlugins[index];

      for (let method of arrReverse(this.layerMethods.postPluginRun)) {
        emitter = method(emitter) || emitter;
      }

      emitter = ((pluginEmit) => (event: PipelineEvent) => {
        if (event.type === 'error') {
          rootEmit(event);
        } else if (!this.runner.aborted || event.type === 'end') {
          pluginEmit(event);
        }
      })(emitter);

      try {
        emitter = plugin.run(emitter) || emitter;
      } catch (err) {
        // It is impossible for the plugin to have emitted the 'end' event
        // at this point because it would have required it to receive it first,
        // which is impossible given that it has thrown an error before
        // eventually returning a listener.

        // TODO: Add metadata here to indicate that this is an abnormal,
        // critical error in the given plugin.
        rootEmit({ type: 'error', error: err });

        // The 'emitter' variable won't be updated so the 'end' event will be
        // forwaded, as we would expect.
      }

      emitter = ((pluginListener) => (event: PipelineEvent) => {
        try {
          pluginListener(event);
        } catch (err) {
          // TODO: Same as above.
          rootEmit({ type: 'error', error: err });

          // If an error was thrown when the provided event was 'end', it is
          // likely (but not sure) that the plugin will fail to re-emit the
          // event. We will therefore handle this here.

          // TODO: Emit a warning explaining this.
          rootEmit({ type: 'end' });
        }
      })(emitter);

      for (let method of arrReverse(this.layerMethods.prePluginRun)) {
        emitter = method(emitter) || emitter;
      }
    }

    for (let method of arrReverse(this.layerMethods.preRun)) {
      emitter = method(emitter) || emitter;
    }

    emitter = ((emit) => (event: PipelineEvent) => {
      if (event.type === 'end') {
        recordedEndEvent = event;
      }

      emit(event);
    })(emitter);

    return emitter;
  }

  subprocess(plugins: Plugins) {
    let processor = new Processor(plugins, { runner: this.runner });

    for (let { layerSource, options } of this.layersCascading) {
      processor.use(layerSource, options);
    }

    this.layerMethods.subprocess.map((method) => method(processor));

    return processor;
  }

  use(layerSource: ProcessorLayerSource, options: Partial<ProcessorLayerOptions> = {}) {
    let layer: Partial<ProcessorLayer> = typeof layerSource === 'function' ? layerSource(this) : layerSource;

    this.layers.push(layer);

    if (options.cascade) {
      this.layersCascading.push({ layerSource, options });
    }

    let isInside = options.place === 'inside'; // so default (void) is outside
    let preMethod: 'push' | 'unshift' = isInside ? 'push': 'unshift';
    let postMethod: 'push' | 'unshift' = isInside ? 'unshift' : 'push';

    // ordered in chronological order
    if (layer.initialize) {
      this.layerMethods.initialize.push(layer.initialize.bind(layer));
    } if (layer.preRun) {
      this.layerMethods.preRun[preMethod](layer.preRun.bind(layer));
    } if (layer.prePluginRun) {
      this.layerMethods.prePluginRun[preMethod](layer.prePluginRun.bind(layer));
    } if (layer.postPluginRun) {
      this.layerMethods.postPluginRun[postMethod](layer.postPluginRun.bind(layer));
    } if (layer.postRun) {
      this.layerMethods.postRun[postMethod](layer.postRun.bind(layer));
    } if (layer.deinitialize) {
      this.layerMethods.deinitialize.push(layer.deinitialize.bind(layer));
    }
  }
}

export interface ProcessorOptions {
  runner: TaskRunner;
}

export interface ProcessorLayer extends Component {
  preRun: ProcessorLayerMethod;
  postRun: ProcessorLayerMethod;

  prePluginRun: ProcessorLayerMethod;
  postPluginRun: ProcessorLayerMethod;

  subprocess(processor: Processor): void;
}

export interface ProcessorLayerOptions {
  cascade: boolean;
  place: 'inside' | 'outside';
}

export type ProcessorLayerMethod = PluginHandler;
export type ProcessorLayerSource = ((processor: Processor) => Partial<ProcessorLayer>) | Partial<ProcessorLayer>;


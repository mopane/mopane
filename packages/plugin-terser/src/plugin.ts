/*!
 * mopane-plugin-terser / plugin
 * Licensed under the EUPL V.1.2
 */


import { DataStream, FocusedPluginHandler } from 'mopane-core';
import { FSSchema } from 'mopane-fs';
import { MinifyOptions, minify } from 'terser';


export function plugin(options: MinifyOptions): FocusedPluginHandler<FSSchema.Event> {
  return (emit) => {
    let deferred: Promise<any>[] = [];

    return (event) => {
      switch (event.type) {
        case 'fs.create':
        case 'fs.update':
          deferred.push(
            event.contents.getBuffer()
              .then((data) => {
                let result = minify({
                  [event.filepath]: data.toString()
                }, options);

                if (result.error) {
                  emit({ type: 'error', error: result.error });
                  return;
                }

                if (!result.code) {
                  return;
                }

                emit({
                  type: event.type,
                  filepath: event.filepath,
                  contents: DataStream.fromString(result.code)
                } as FSSchema.FileCreationEvent | FSSchema.FileUpdateEvent);
              })
          );

          break;

        case 'end':
          Promise.all(deferred)
            .catch((error) => {
              emit({ type: 'error', error });
            })
            .then(() => {
              emit(event);
            });
      }
    };
  };
}


module.exports = {
  moduleFileExtensions: ['ts', 'js'],
  preset: 'ts-jest',
  testMatch: [__dirname + '/packages/*/tests/*.ts'],

  collectCoverageFrom: ['packages/*/src/**/*.(js|ts)'],
  coverageDirectory: 'coverage',
  coverageReporters: ['html', 'text']
};

/*!
 * mopane-fs / mergeFileEvents()
 * Licensed under the EUPL V.1.2
 */


import { FSSchema } from './schemas/fs';


type FSEvent = FSSchema.Event;

export function mergeFileEvents(events: FSEvent[], extraEvents: FSEvent[], filepath: string): FSEvent[] {
  return extraEvents.reduce((events, extraEvent) => mergeFileEvent(events, extraEvent, filepath), events);
}

export function mergeFileEvent(events: FSEvent[], extraEvent: FSEvent, filepath: string): FSEvent[] {
  if (events.length < 1) {
    return [extraEvent];
  }

  let createEvent: FSSchema.FileCreationEvent | null = null;
  let updateEvent: FSSchema.FileUpdateEvent | null = null;
  let deleteEvent: FSSchema.FileDeletionEvent | null = null;

  for (let event of events) {
    switch (event.type) {
      case 'fs.create':
        createEvent = event;
        break;

      case 'fs.update':
        updateEvent = event;
        break;

      case 'fs.delete':
        deleteEvent = event;
        break;
    }
  }


  switch (extraEvent.type) {
    case 'fs.create':
      // remove (status 3) + create => update
      if (deleteEvent) {
        return [{
          type: 'fs.update',
          filepath,
          contents: extraEvent.contents,
          mode: extraEvent.mode
        }];
      }

      throw new Error('Could not merge fs.create to other events');

    case 'fs.update':
      // create (status 1) + update => create
      if (createEvent) {
        return [{
          ...createEvent,
          contents: extraEvent.contents
        }];
      }

      // update (status 2) + update => update
      if (updateEvent) {
        return [{
          ...updateEvent,
          contents: extraEvent.contents
        }];
      }

      throw new Error('Could not merge fs.update to other events');

    case 'fs.delete':
      // create (status 1) + delete => n/a
      // update (status 2) + delete => n/a
      if (!deleteEvent) {
        return [];
      }

      throw new Error('Could not merge fs.delete to other events');
  }
}


/*!
 * mopane-fs / fs schema
 * Licensed under the EUPL V.1.2
 */


import { DataStream, PipelineEvent } from 'mopane-core';


export namespace FSSchema {
  export type Event = FileCreationEvent | FileDeletionEvent | FileUpdateEvent;

  export interface FileCreationEvent extends PipelineEvent {
    type: 'fs.create';

    filepath: string;
    contents: DataStream;
    mode?: number | null;
  }

  export interface FileDeletionEvent extends PipelineEvent {
    type: 'fs.delete';

    filepath: string;
  }

  export interface FileUpdateEvent extends PipelineEvent {
    type: 'fs.update';

    filepath: string;
    contents: DataStream;
    mode?: number | null;
  }


  export function isEvent(event: PipelineEvent): event is Event {
    return typeof event.type === 'string' && event.type.startsWith('fs.');
  }
}


/*!
 * mopane-builtins / split plugin
 * Licensed under the EUPL V.1.2
 */


import { PipelineEvent, Plugin, Plugins, Processor } from 'mopane-core';


export function split(...handlers: Handler[]): Plugin {
  let processors: Processor[];

  return {
    async initialize(context) {
      processors = handlers.map((handler) => context.processor.subprocess(handler()));

      await Promise.all(
        processors.map((processor) => processor.initialize())
      );
    },
    run(emit) {
      let failed = false;
      let subprocessorsDone = 0;

      let emitter = (event: PipelineEvent) => {
        switch (event.type) {
          case 'end':
            if (++subprocessorsDone >= processors.length) {
              emit(event);
            }

            break;

          case 'error':
            if (failed) break;

            failed = true;
            // fallthrough is voluntary

          default:
            emit(event);
        }
      };

      let listeners = processors.map((processor) => processor.run(emitter));

      return (event) => {
        for (let listener of listeners) {
          listener(event);
        }
      };
    },
    async deinitialize() {
      await Promise.all(
        processors.map((processor) => processor.deinitialize())
      );
    },

    async commit() {
      await Promise.all(
        processors.map((processor) => processor.commit())
      );
    },
    async discard() {
      await Promise.all(
        processors.map((processor) => processor.discard())
      );
    }
  };
}

type Handler = () => Plugins;


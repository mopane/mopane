/*!
 * mopane-core / Interval
 * Licensed under the EUPL V.1.2
 */


/**
 * accepted inputs :
 *  - new Interval(3)           (degenerate)
 *  - new Interval(2, 5)        (bounded)
 *  - new Interval(3, null)     (left-bounded)
 *  - new Interval(null, 5)     (right-bounded)
 *  - new Interval()            (empty)
 *  - new Interval(null)        (proper)
 */
export abstract class Interval {
  abstract inspect(): string;
  abstract matchesValue(test: number): boolean;
  abstract get size(): number; // largest size possible (can be higher than reality)
  abstract get values(): number[];

  apply(tests: number[] | object): number[] {
    if (!Array.isArray(tests)) {
      return this.apply(
        Object.keys(tests)
          .map((value) => typeof value === 'number' ? value : parseInt(value))
      );
    }

    return tests.filter((value) => this.matchesValue(value));
  }

  intersect(...intvs: Interval[]): Interval {
    return new IntersectionInterval(this, ...intvs);
  }

  // true negatives may exist, but not false positives
  isFinite(): boolean {
    return true;
  }

  negate(): Interval {
    return new NegationInterval(this);
  }

  substract(...intvs: Interval[]): Interval {
    return new IntersectionInterval(
      this,
      new UnionInterval(...intvs).negate()
    );
  }

  unite(...intvs: Interval[]): Interval {
    return new UnionInterval(this, ...intvs);
  }

  static create(): EmptyInterval;
  static create(a: null): ProperInterval;
  static create(a: number): DegenerateInterval;
  static create(a: number, b: null): LeftBoundedInterval;
  static create(a: null, b: number): RightBoundedInterval;
  static create(a: number, b: number): BoundedInterval;

  static create(a?: null | number, b?: null | number): Interval {
    if (a === void 0 && b === void 0) return new EmptyInterval();
    if (a === null && b === void 0) return new ProperInterval();
    if (typeof a === 'number' && b === void 0) return new DegenerateInterval(a);
    if (typeof a === 'number' && b === null) return new LeftBoundedInterval(a);
    if (a === null && typeof b === 'number') return new RightBoundedInterval(b);
    if (typeof a === 'number' && typeof b === 'number') return new BoundedInterval(a, b);

    return new EmptyInterval();
  }

  static from(value: Interval | number | null): Interval {
    return typeof value === 'object'
      ? value as Interval
      : Interval.create(value);
  }
}


export class BoundedInterval extends Interval {
  private leftBound: number;
  private rightBound: number;

  constructor(leftBound: number, rightBound: number) {
    super();

    this.leftBound = leftBound;
    this.rightBound = rightBound;
  }

  get size() {
    return this.rightBound - this.leftBound + 1;
  }

  get values() {
    let values = [];

    for (let value = this.leftBound; value < this.rightBound + 1; value++) {
      values.push(value);
    }

    return values;
  }

  isFinite(): boolean {
    return true;
  }

  matchesValue(test: number) {
    return test >= this.leftBound && test <= this.rightBound;
  }

  negate() {
    return new UnionInterval(
      new RightBoundedInterval(this.leftBound - 1),
      new LeftBoundedInterval(this.rightBound + 1)
    );
  }

  inspect() {
    return `[${this.leftBound} ; ${this.rightBound}]`;
  }
}

export class DegenerateInterval extends Interval {
  private value: number;

  constructor(value: number) {
    super();

    this.value = value;
  }

  get size() {
    return 1;
  }

  get values() {
    return [this.value];
  }

  isFinite(): boolean {
    return true;
  }

  matchesValue(test: number) {
    return test === this.value;
  }

  negate() {
    return new BoundedInterval(this.value, this.value).negate();
  }

  inspect() {
    return `{${this.value}}`;
  }
}

export class EmptyInterval extends Interval {
  get size() {
    return 0;
  }

  get values() {
    return [];
  }

  intersect(...intvs: Interval[]) {
    return this;
  }

  isFinite(): boolean {
    return true;
  }

  matchesValue(test: number) {
    return false;
  }

  negate() {
    return new ProperInterval();
  }

  unite(...intvs: Interval[]) {
    return new UnionInterval(...intvs);
  }

  inspect() {
    return '∅';
  }
}

export class IntersectionInterval extends Interval {
  private intervals: Interval[];

  constructor(...intvs: Interval[]) {
    super();

    this.intervals = intvs;
  }

  get size() {
    return Math.max(...this.intervals.map((intv) => intv.size));
  }

  get values() {
    let finiteIntv = this.intervals.find((intv) => intv.isFinite());

    if (finiteIntv === void 0) {
      throw new Error('Interval is infinite');
    }

    return finiteIntv.values.filter((value) => {
      for (let intv of this.intervals) {
        if (intv === finiteIntv) {
          continue;
        }

        if (!intv.matchesValue(value)) {
          return false;
        }
      }

      return true;
    });
  }

  isFinite(): boolean {
    return this.intervals.some((intv) => intv.isFinite());
  }

  matchesValue(test: number) {
    return this.intervals.every((intv) => intv.matchesValue(test));
  }

  negate() { // Morgan's law
    return new UnionInterval(
      ...this.intervals.map((intv) => intv.negate())
    );
  }

  inspect() {
    return '(' + this.intervals.map((intv) => intv.inspect()).join(' ∩ ') + ')';
  }
}

export class LeftBoundedInterval extends Interval {
  private bound: number;

  constructor(bound: number) {
    super();

    this.bound = bound;
  }

  get size() {
    return Infinity;
  }

  get values(): number[] {
    throw new Error('Interval is infinite');
  }

  isFinite(): boolean {
    return false;
  }

  matchesValue(test: number) {
    return test >= this.bound;
  }

  negate() {
    return new RightBoundedInterval(this.bound - 1);
  }

  inspect() {
    return `[${this.bound} ; +∞[`;
  }
}

export class NegationInterval extends Interval {
  private interval: Interval;

  constructor(intv: Interval) {
    super();

    this.interval = intv;
  }

  get size() {
    return Infinity;
  }

  get values(): number[] {
    throw new Error('Interval is possibly infinite');
  }

  isFinite(): boolean {
    return false;
  }

  matchesValue(test: number) {
    return !this.interval.matchesValue(test);
  }

  inspect() {
    return `/(${this.interval.inspect()})`;
  }
}

export class ProperInterval extends Interval {
  get size() {
    return Infinity;
  }

  get values(): number[] {
    throw new Error('Interval is infinite');
  }

  intersect(...intvs: Interval[]) {
    return new IntersectionInterval(...intvs);
  }

  isFinite(): boolean {
    return false;
  }

  matchesValue(test: number) {
    return true;
  }

  negate() {
    return new EmptyInterval();
  }

  unite(...intvs: Interval[]) {
    return this;
  }

  inspect() {
    return `]-∞ ; +∞[`;
  }
}

export class RightBoundedInterval extends Interval {
  private bound: number;

  constructor(bound: number) {
    super();

    this.bound = bound;
  }

  get size() {
    return Infinity;
  }

  get values(): number[] {
    throw new Error('Interval is infinite');
  }

  isFinite(): boolean {
    return false;
  }

  matchesValue(test: number) {
    return test <= this.bound;
  }

  negate() {
    return new LeftBoundedInterval(this.bound + 1);
  }

  inspect() {
    return `]-∞ ; ${this.bound}]`;
  }
}

export class UnionInterval extends Interval {
  private intervals: Interval[];

  constructor(...intvs: Interval[]) {
    super();

    this.intervals = intvs;
  }

  get size() {
    return this.intervals.reduce((sum, intv) => sum + intv.size, 0);
  }

  get values() {
    let values = new Set();

    for (let intv of this.intervals) {
      for (let value of intv.values) {
        values.add(value);
      }
    }

    return Array.from(values);
  }

  isFinite(): boolean {
    return this.intervals.every((intv) => intv.isFinite());
  }

  matchesValue(test: number) {
    return this.intervals.some((intv) => intv.matchesValue(test));
  }

  negate() { // Morgan's law
    return new IntersectionInterval(
      ...this.intervals.map((intv) => intv.negate())
    );
  }

  inspect() {
    return '(' + this.intervals.map((intv) => intv.inspect()).join(' ∪ ') + ')';
  }
}

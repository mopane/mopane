/*!
 * mopane-adapter-gulp / adapter
 * Licensed under the EUPL V.1.2
 */


import * as Vinyl from 'vinyl';
import { DataStream, Plugin, PluginContext } from 'mopane-core';
import { FSSchema, absolutizePath, relativizePath } from 'mopane-fs';
import { Duplex as DuplexStream, Readable as ReadableStream } from 'stream';

import { DefaultFileStore, FileStore, SafeFileStore } from './file-store';


export function adapter(gulpPlugin: GulpPlugin, options: Options = {}): Plugin {
  let Store = options.safe ? SafeFileStore : DefaultFileStore;
  let store: FileStore = new Store();

  let context: PluginContext | null = null;

  return {
    initialize(ctx) {
      context = ctx;
    },

    run(emit) {
      let root = (context as PluginContext).project.root;

      let inputStream = new ReadableStream({
        objectMode: true,
        read() {}
      });

      let inputDeferred: Promise<void>[] = [];

      let stream = gulpPlugin();
      inputStream.pipe(stream);

      stream.on('data', (file: Vinyl) => {
        if (file.isDirectory() || file.isNull()) return;

        let storeFilepath = relativizePath(file.history[0], root);

        let event = {
          type: store.pushOutput(storeFilepath, file.relative) ? 'fs.create' : 'fs.update',
          filepath: file.relative,

          contents: file.isStream()
            ? DataStream.fromStream(file.contents as ReadableStream)
            : DataStream.fromBuffer(Buffer.from(file.contents as Buffer | string)),

          channel: 0
        } as FSSchema.FileCreationEvent | FSSchema.FileUpdateEvent;

        // console.log('--> from', relativizePath(file.history[0], root), 'to', file.relative)

        emit(event);
      });

      let outputEndPromise = new Promise((resolve) => {
        stream.on('error', (error) => {
          emit({ type: 'error', error });
          resolve();
        });

        stream.on('end', () => {
          resolve();
        });
      });

      return (e) => {
        switch (e.type) {
          case 'fs.create':
          case 'fs.update': {
            let event = e as FSSchema.FileCreationEvent | FSSchema.FileUpdateEvent;

            if (event.channel !== 0) break;

            inputDeferred.push((async () => {
              let file = new Vinyl({
                base: root,
                path: absolutizePath(event.filepath, root),
                contents: options.disableStreaming
                  ? await event.contents.getBuffer()
                  : await event.contents.getStream()
              });

              store.pushInput(event.filepath);
              inputStream.push(file);
            })());

            return;
          }

          case 'fs.delete':
            store.deleteInput((e as FSSchema.FileDeletionEvent).filepath);
            return;

          case 'end':
            Promise.all(inputDeferred)
              .then(() => {
                inputStream.push(null);

                return outputEndPromise;
              })
              .then(() => {
                for (let filepath of store.retreiveDeleted()) {
                  emit<FSSchema.FileDeletionEvent>({
                    type: 'fs.delete',
                    filepath,

                    channel: 0
                  });
                }

                emit(e);
              });

            return;
        }

        emit(e);
      };
    },

    commit() {
      store.commit();
    },

    discard() {
      store.discard();
    }
  };
}

interface Options {
  disableStreaming?: boolean;
  safe?: boolean;
}


type GulpPlugin = () => DuplexStream;


/*!
 * mopane-cli / main
 * Licensed under the EUPL V.1.2
 */


import * as minimist from 'minimist';
import * as path from 'path';
import { BuiltinDrivers, DriverName, EnhancedError, Project, Task, TaskRunner } from 'mopane-core';
import { ReadLine, createInterface } from 'readline';

import { Controller, ControllerScreen } from './controller';
import { RunnerWrapper } from './runner-wrapper';


// Possible statuses are:
//   - running: the pipeline is running
//   - committing: the pipeline has done running is a committing/discarding its results
//   - stopping
//   - idle: the pipeline is waiting for an interrupt
//   - paused: the pipeline was paused by the user
//   - waiting: the pipeline was paused due to the client typing something / or waiting for the user's input
//


interface Tasks {
  [name: string]: Task;
}

export abstract class CLIContext<Parent extends CLIContext<any> | CLI> {
  constructor(readonly parent: Parent) {

  }

  get controller(): Controller {
    return this.parent.controller;
  }

  get root(): CLI {
    return this.parent.root;
  }

  enter() {
    this.root.context = this;
  }

  onSIGINT() {

  }
}

export class CLI {
  arguments: { [key: string]: string; };

  controller: Controller = new Controller(process.stdin, process.stdout);
  context: { onSIGINT(): void; };
  logScreen: ControllerScreen;

  constructor() {
    let argv = process.argv.slice(2);

    let args = minimist(argv);
    let command = args._[0];

    let home = new HomeContext(this);
    this.context = home;

    this.arguments = args;
    this.logScreen = this.controller.createScreen();

    this.controller.intf.on('SIGINT', () => {
      this.context.onSIGINT();
    });

    if (command) {
      home.processCommand(args);
    } else {
      home.enterFromParent();
    }
  }

  get root() {
    return this;
  }

  enterFromChild() {
    this.controller.close();
  }
}

export class HomeContext extends CLIContext<CLI> {
  loadTasks(confPath: string): Tasks {
    try {
      return require(confPath);
    } catch (err) {
      if (err.code === 'MODULE_NOT_FOUND' && err.message.startsWith(`Cannot find module '${confPath}'`)) {
        throw new Error('Missing configuration file');
      }

      throw err;
      // throw new Error('Invalid configuration file');
    }
  }

  processCommand(args: ReturnType<typeof minimist>) {
    let command = args._[0];

    this.root.logScreen.contents = '';

    switch (command) {
      case 'load':
        let rootPath = makePathAbsolute(args.root || this.root.arguments.root || '.', process.cwd());
        let confPath = makePathAbsolute(args._[1] || args.config || 'Mopanefile.js', rootPath);

        try {
          let tasks = this.loadTasks(confPath);
          this.controller.message = `Loaded ${Object.keys(tasks).length} tasks from ${path.relative(process.cwd(), confPath)}.`;

          new ProjectContext(this, tasks, rootPath).enterFromParent();
        } catch (err) {
          this.controller.message = `Failed loading configuration: ${err.message}.`;
          this.prompt();

          this.root.logScreen.contents = err.stack + '\n';
        }

        break;

      default:
        this.controller.message = `Unknown command '${command}'. Use 'help' for a list of commands.`;
        this.prompt();

        return;
    }
  }

  async prompt() {
    this.processCommand(minimist((await this.controller.prompt()).split(' ')));
  }

  enterFromChild() {
    super.enter();

    this.controller.message = 'Unloaded project.';
    this.controller.status = 'context: home';
    this.prompt();
  }

  enterFromParent() {
    super.enter();

    this.controller.message = 'Ready.';
    this.controller.status = 'context: home';
    this.prompt();
  }

  onSIGINT() {
    this.parent.enterFromChild();
  }
}


export class ProjectContext extends CLIContext<HomeContext> {
  project: Project;

  constructor(parent: HomeContext, readonly tasks: Tasks, rootPath: string) {
    super(parent);

    this.project = new Project({ root: rootPath });
  }

  async prompt() {
    let value = await this.controller.prompt();
    let args = minimist(value.split(' '));
    let command = args._[0];

    this.root.logScreen.contents = '';

    switch (command) {
      case 'return':
        this.parent.enterFromChild();
        return;

      case 'start':
        let name = args._[1];

        if (!name) {
          this.controller.message = 'Missing task name.';
          break;
        }

        let task = this.tasks[name];

        if (!task) {
          this.controller.message = `Unknown task '${name}'.`;
          break;
        }

        new TaskContext(this, name, task, this.project).enter();

        return;

      default:
        this.controller.message = `Unknown command '${command}'.`;
        break;
    }

    this.prompt();
  }

  enterFromChild() {
    super.enter();

    this.controller.message = 'Returned to project.';
    this.controller.status = 'context: project';
    this.prompt();
  }

  enterFromParent() {
    super.enter();

    this.controller.status = 'context: project';
    this.prompt();
  }

  onSIGINT() {
    this.parent.onSIGINT();
  }
}


export class TaskContext extends CLIContext<ProjectContext> {
  runner: TaskRunner;
  wrapper: RunnerWrapper;

  constructor(parent: ProjectContext, readonly name: string, readonly task: Task, project: Project) {
    super(parent);

    this.runner = project.startTaskRunner(this.task, { persistent: false });
    this.wrapper = new RunnerWrapper(this.runner);
  }

  async prompt() {
    let value = await this.controller.prompt();
    let args = minimist(value.split(' '));
    let command = args._[0];

    this.root.logScreen.contents = '';

    switch (command) {
      case 'return':
        this.parent.enterFromChild();
        return;

      case 'run':
        this.controller.message = 'Running';
        this.controller.spinning = true;

        this.wrapper.run()
          .on('done', () => {
            this.controller.message = 'Done.';
            this.controller.spinning = false;
          })
          .on('error', (err) => {
            this.controller.message = `Run failed: ${err.message}.`;
            this.controller.spinning = false;

            this.root.logScreen.contents = err.stack + '\n';
          })
          .on('abort', () => {
            this.controller.message = 'Aborting run';
          })
          .on('aborted', () => {
            this.controller.message = 'Aborted run.';
            this.controller.spinning = false;
          });

        break;
    }

    this.prompt();
  }

  enter() {
    super.enter();

    this.controller.message = `Loaded task '${this.name}'.`;
    this.controller.status = `context: task '${this.name}'`;
    this.prompt();

    this.controller.message = 'Initializing';
    this.controller.spinning = true;

    this.wrapper.initialize()
      .on('done', () => {
        this.controller.message = 'Initialized task.';
        this.controller.spinning = false;
      })
      .on('error', (err) => {
        this.controller.message = `Initialization failed: ${err.message}.`;
        this.controller.spinning = false;

        this.root.logScreen.contents = err.stack + '\n';
        this.parent.enterFromChild();
      })
      .on('abort', () => {
        this.controller.message = 'Aborting initialization (1/2)';
      })
      .on('aborted.initialization', () => {
        this.controller.message = 'Aborting initialization (2/2)';
      })
      .on('aborted', () => {
        this.controller.message = 'Aborted initialization of task.';
        this.controller.spinning = false;

        this.parent.enterFromChild();
      });
  }

  onSIGINT() {
    if (!this.wrapper.abort()) {
      this.parent.enterFromChild();
    }
  }
}


export class _TaskContext {
  controller: Controller;
  parent: CLI;

  project: Project;
  tasks: { [taskName: string]: Task; } = {};
  wrapper: RunnerWrapper | null = null;

  constructor(controller: Controller, parent: CLI, taskName: string, args: ReturnType<typeof minimist>) {
    let root = makePathAbsolute(args.root || '.', process.cwd());
    let confPath = makePathAbsolute(args.config || 'Mopanefile.js', root);

    this.project = new Project({ root });

    this.controller = controller;
    this.parent = parent;


    this.loadTasks(confPath);
    this.controller.message = `Loaded ${Object.keys(this.tasks).length} tasks from ${path.relative(process.cwd(), confPath)}`;


    this.controller.intf.on('SIGINT', () => {
      if (this.wrapper) {
        if (!this.wrapper.abort()) {
          this.controller.close();
          process.exit(1);
        }
      } else {
        this.controller.close();
      }
    });

    this.prompt();
  }

  exitWithError(err: Error) {
    this.controller.close();

    console.error(err.message);
    console.error(err.stack);

    process.exit(1);
  }

  loadTasks(confPath: string) {
    try {
      this.tasks = require(confPath);
    } catch (err) {
      if (err.code === 'MODULE_NOT_FOUND' && err.message.startsWith(`Cannot find module '${confPath}'`)) {
        throw new Error('Missing configuration file');
      }

      throw err;
      // throw new Error('Invalid configuration file');
    }
  }


  async runCommand(taskName: string) {
    if (!(taskName in this.tasks)) {
      throw new Error(`Missing task '${taskName}'`);
      // this.controller.message = `Missing task '${taskName}'`;
      return;
    }

    let task = this.tasks[taskName];

    let runner = this.project.startTaskRunner(task, { persistent: false });

    await runner.profile.registerDrivers({
      fs: BuiltinDrivers.fs,
      http: BuiltinDrivers.http,
      https: BuiltinDrivers.https
    });


    this.controller.status = 'loading';
    this.controller.message = 'Initializing';
    this.controller.spinning = true;


    this.wrapper = new RunnerWrapper(runner);

    this.controller.message = 'Initializing';
    this.controller.spinning = true;

    this.wrapper.initialize()
      .on('done', () => {
        this.controller.message = 'Done';
        this.wrapper = null;
      })
      .on('error', (err) => {
        this.controller.message = 'Initialization failed: ' + err.message;
        this.controller.spinning = false;
        this.wrapper = null;
      })
      .on('abort', () => {
        this.controller.message = 'Aborting initialization (1/2)';
      })
      .on('aborted.initialization', () => {
        this.controller.message = 'Aborting initialization (2/2)';
      })
      .on('aborted', () => {
        this.controller.message = 'Aborted initialization';
        this.controller.spinning = false;
        this.wrapper = null;
      });

    /* let wrapper = new RunnerWrapper(runner);

    this.controller.message = 'Initializing';
    wrapper.initialize();

    wrapper.once('initialized', () => {
      this.controller.message = 'Initialized';
    });

    wrapper.run(); */
  }


  async prompt() {
    let value = await this.controller.prompt();
    let args = minimist(value.split(' '));

    let command = args._[0];


    switch (command) {
      case 'close':
        this.parent.childExit();
        return;

      case 'exit':
        this.controller.close();
        return;
      case 'start':
        this.runCommand(args._[1]);
        break;

      default:
        this.controller.message = `Unknown command '${command}'`;
    }

    this.prompt();

/* this.controller.message = `Unknown command '${value}'`;
    this.controller.status = 'failed';

    if (value === 'exit') {
      this.controller.close();
      return;
    }

    setTimeout(() => {
      this.controller.status = null;
    }, 800);

    await this.prompt(); */
  }
}


function makePathAbsolute(target: string, root: string): string {
  return path.isAbsolute(target)
    ? target
    : path.join(root, target);
}


# Mopane

[![build status](https://gitlab.com/mopane/mopane/badges/master/build.svg)](https://gitlab.com/mopane/mopane/commits/master)
[![coverage report](https://gitlab.com/mopane/mopane/badges/master/coverage.svg)](https://gitlab.com/mopane/mopane/commits/master)


**Mopane is a build tool and task runner** faster and simpler than Grunt, Gulp, Brunch, Broccoli, Webpack and others.


## Installation

This package is not intended to be installed for production.


## Development

`package-lock.json` is ignored in Git and should only be used during developement, for example to reproduce bugs or benchmarks. External dependencies will normally be installed at the root of the repository to avoid duplicates.

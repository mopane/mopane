/*!
 * mopane-core / TaskBuilder
 * Licensed under the EUPL V.1.2
 */


import { Plugin } from './plugin';
import { Project } from './project';
import { TaskRunner } from './task-runner';
import { PossiblyDeferred } from './util';


export class TaskBuilder {
  configurator: TaskBuilderConfigurator;

  constructor(configurator: TaskBuilderConfigurator = () => {}) {
    this.configurator = configurator;
  }

  config(altConfigurator: TaskBuilderConfigurator) {
    let configurator = this.configurator;

    return new TaskBuilder(async function (args) {
      await configurator.call(this, args);
      await altConfigurator.call(this, args);
    });
  }

  task(handler: TaskBuilderHandler) {
    return new Task(handler, this.configurator);
  }
}

export interface TaskBuilderContext {
  project: Project;
  runner: TaskRunner;
}

export type TaskBuilderConfigurator = (this: TaskBuilderContext, args: object) => PossiblyDeferred<void>;
export type TaskBuilderHandler = (args: object) => Iterable<Plugin>;


export class Task {
  configurator: TaskBuilderConfigurator;
  handler: TaskBuilderHandler;

  constructor(handler: TaskBuilderHandler, configurator: TaskBuilderConfigurator) {
    this.configurator = configurator;
    this.handler = handler;
  }

  async prepare(context: TaskBuilderContext, args: object) {
    await this.configurator.call(context, args);
    return await this.handler(args);
  }
}


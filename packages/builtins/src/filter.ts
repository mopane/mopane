/*!
 * mopane-builtins / filter plugin
 * Licensed under the EUPL V.1.2
 */


import { Interval, PipelineEvent, Plugin } from 'mopane-core';


export function filter(filter: Filter, options: FilterPluginOptions = {}): Plugin {
  const unfilteredEventTypes = [
    'end',
    'ch.active', 'ch.close'
  ];

  return function (emit) {
    return (event) => {
      // TODO: remove #toString()
      if (
        unfilteredEventTypes.indexOf(event.type.toString()) >= 0
        || options.channels && event.channel !== void 0 && !options.channels.matchesValue(event.channel)
        || filter(event)
      ) {
        emit(event);
      }
    };
  };
}

export interface FilterPluginOptions {
  channels?: Interval;
}

// TODO: allow returning a promise
export type Filter = (event: PipelineEvent) => boolean;


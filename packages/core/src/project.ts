/*!
 * mopane-core / Core
 * Licensed under the EUPL V.1.2
 */


import { Task } from './task-builder';
import { TaskRunner, TaskRunnerOptions } from './task-runner';


export class Project {
  root: string;

  constructor(options: ProjectOptions) {
    this.root = options.root;
  }

  startTaskRunner(task: Task, options?: TaskRunnerOptions) {
    return new TaskRunner(task, this, options);
  }
}

export interface ProjectOptions {
  root: string;
}


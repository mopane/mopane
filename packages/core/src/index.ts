/*!
 * mopane-core
 * Licensed under the EUPL V.1.2
 */


import { TaskBuilder } from './task-builder';


export const version = '0.0.0';

export * from './component';
export * from './data-stream';
export * from './enhanced-error';
export * from './drivers';
export * from './interval';
export * from './lada';
export * from './plugin';
export * from './processor';
export * from './project';
export * from './profile';
export * from './schema-guard';
export * from './schemas/channel';
export * from './schemas/root';
export * from './task-builder';
export * from './task-runner';


const rootBuilder = new TaskBuilder();

let config = rootBuilder.config.bind(rootBuilder);
let task = rootBuilder.task.bind(rootBuilder);

export { config, task };


/*!
 * mopane-plugin-js-validator
 * Licensed under the EUPL V.1.2
 */


import * as esprima from 'esprima';
import { EnhancedError, FocusedPluginHandler } from 'mopane-core';
import { FSSchema } from 'mopane-fs';


export function plugin(): FocusedPluginHandler<FSSchema.Event> {
  return (emit) => {
    let deferred: Promise<void>[] = [];

    return (event) => {
      switch (event.type) {
        case 'fs.create':
        case 'fs.update':
          deferred.push(
            event.contents.getBuffer()
              .then((contents) => {
                let errors: Error[] = [];

                try {
                  let syntax = esprima.parseScript(contents.toString(), { comment: true, tolerant: true, tokens: true });

                  errors = (syntax as unknown as { errors: Error[] }).errors;
                } catch (err) {
                  errors = [err];
                }

                if (errors.length < 1) {
                  return;
                }

                let allLines = contents.toString().split('\n');

                for (let _err of errors) {
                  let err = _err as Error & { index: number; lineNumber: number; };

                  let startLineIndex = Math.max(err.lineNumber - 1, 1);
                  let lines = allLines.slice(startLineIndex - 1, startLineIndex + 2);

                  let columnIndex = err.index - allLines
                    .filter((line, index) => index < err.lineNumber - 1)
                    .reduce((total, line) => total + line.length + 1, 0);

                  // For mysterious reasons, the err.index sometimes does not
                  // match err.lineNumber, resulting in a negative columnIndex.
                  columnIndex = Math.max(columnIndex, 0);

                  let error = new EnhancedError.Error({
                    kind: 'ESCMAScript parsing error',
                    message: err.message,
                    description: [
                      `An error occured while parsing *${event.filepath}*. It is as follows:`,
                      { type: EnhancedError.NodeType.Quote, value: err.message },
                      'The corresponding source code is:',
                      { type: EnhancedError.NodeType.SourceCode,
                        language: 'javascript',
                        value: lines.map((value, relativeIndex) => {
                          let index = startLineIndex + relativeIndex;

                          return { index, value,
                            underlining: index === err.lineNumber ? [columnIndex, columnIndex + 1] as [number, number] : void 0 };
                        }) }
                    ]
                  });

                  emit({ type: 'error', error });
                }
              })
          );

          break;

        case 'end':
          Promise.all(deferred)
            .catch((error) => {
              emit({ type: 'error', error });
            })
            .then(() => {
              emit(event);
            });

          return;
      }

      emit(event);
    };
  };
}


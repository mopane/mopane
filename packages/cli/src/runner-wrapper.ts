/*!
 * mopane-cli / runner-wrapper
 * Licensed under the EUPL V.1.2
 */


import { EventEmitter } from 'events';
import { TaskRunner } from 'mopane-core';


export class RunnerWrapper {
  runner: TaskRunner;

  eventEmitter: EventEmitter | null = null;
  status: Status = Status.None;

  constructor(runner: TaskRunner) {
    this.runner = runner;
  }

  abort(): boolean {
    switch (this.status) {
      case Status.Deinitializing:
        this.status = Status.DeinitializingAborting;
        this.eventEmitter!.emit('abort');

        break;

      case Status.Initializing:
        this.status = Status.InitializingAborting;
        this.eventEmitter!.emit('abort');

        break;

      case Status.Running:
        this.status = Status.RunningAborting;
        this.eventEmitter!.emit('abort');

        break;

      default:
        return false;
    }

    return true;
  }

  deinitialize() {
    this.eventEmitter = new EventEmitter();
    this.status = Status.Deinitializing;

    this.runner.deinitialize()
      .then(() => {
        switch (this.status) {
          case Status.Deinitializing:
            this.status = Status.None;
            this.eventEmitter!.emit('done');

            break;

          case Status.DeinitializingAborting:
            this.status = Status.None;
            this.eventEmitter!.emit('aborted');

            break;
        }
      })
      .catch((err) => {
        if (this.status === Status.Deinitializing) {
          this.eventEmitter!.emit('error', err);
        }

        this.status = Status.None;
      });
  }

  initialize() {
    this.eventEmitter = new EventEmitter();
    this.status = Status.Initializing;

    this.runner.initialize()
      .then(() => {
        switch (this.status) {
          case Status.Initializing:
            this.status = Status.Idle;
            this.eventEmitter!.emit('done');
            break;

          case Status.InitializingAborting:
            this.status = Status.InitializingAbortingDeinitializing;
            this.eventEmitter!.emit('aborted.initialization');

            this.runner.deinitialize()
              .catch((err) => {})
              .then(() => {
                this.status = Status.None;
                this.eventEmitter!.emit('aborted');
              });

            break;
        }
      })
      .catch((err) => {
        if (this.status === Status.Initializing) {
          this.eventEmitter!.emit('error', err);
        }

        this.status = Status.None;
      });

    return this.eventEmitter;
  }

  run() {
    this.eventEmitter = new EventEmitter();

    if (this.status !== Status.Idle) {
      this.eventEmitter.emit('error', new Error('Not ready'));
      return this.eventEmitter;
    }

    this.status = Status.Running;

    this.runner.run()
      .then(() => {
        switch (this.status) {
          case Status.Running:
            this.status = Status.Idle;
            this.eventEmitter!.emit('done');
            break;

          case Status.RunningAborting:
            this.status = Status.Idle;
            this.eventEmitter!.emit('aborted');
            break;
        }
      })
      .catch((err) => {
        if (this.status === Status.Running) {
          this.eventEmitter!.emit('error', err);
        }

        this.status = Status.Idle;
      });

    return this.eventEmitter;
  }
}

enum Status {
  Deinitializing, DeinitializingAborting, Idle, Initializing, InitializingAborting, InitializingAbortingDeinitializing, None, Running, RunningAborting
}


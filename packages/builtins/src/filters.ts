/*!
 * mopane-builtins / filters
 * Licensed under the EUPL V.1.2
 */


import * as minimatch from 'minimatch';
import { Interval, PipelineEvent } from 'mopane-core';
import { FSSchema } from 'mopane-fs';

import { Filter } from './filter';


export namespace Filters {
  export function and(...filters: Filter[]): Filter {
    return (event: PipelineEvent) => filters.every((filter) => filter(event));
  }

  export function or(...filters: Filter[]): Filter {
    return (event: PipelineEvent) => filters.some((filter) => filter(event));
  }

  export function not(filter: Filter): Filter {
    return (event: PipelineEvent) => !filter(event);
  }


  export function isInInterval(intv: Interval): Filter {
    return (event: PipelineEvent) => event.channel !== void 0 && intv.matchesValue(event.channel);
  }

  export function matchFiles(
    pattern: RegExp | string | ((fullPath: string) => boolean),
    patternOptions: object = {}
  ): Filter {
    if (typeof pattern === 'string') {
      let regexp = minimatch.makeRe(pattern, patternOptions);

      if (!regexp) {
        throw new Error(`Invalid minimatch pattern '${pattern}'`);
      }

      pattern = regexp;
    }

    let testHandler = pattern instanceof RegExp
      ? (fullPath: string) => (pattern as RegExp).test(fullPath)
      : pattern;

    return (event: PipelineEvent) => {
      if (!FSSchema.isEvent(event)) {
        return true;
      }

      return testHandler(event.filepath);
    };
  }
}


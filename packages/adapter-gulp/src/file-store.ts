/*!
 * mopane-adapter-gulp / FileStore
 * Licensed under the EUPL V.1.2
 */


export interface FileStore {
  deleteInput(filepath: string): void;
  pushInput(filepath: string): void;
  pushOutput(inputFilepath: string, outputFilepath: string): boolean;
  retreiveDeleted(): string[];

  commit(): void;
  discard(): void;
}


export class DefaultFileStore implements FileStore {
  private files: {
    [filepath: string]: {
      added: boolean;
      updated: boolean;
      deleted: boolean;

      pushed: boolean;
      pushedPath: string | null;
    }
  } = {};

  deleteInput(filepath: string) {
    this.files[filepath].deleted = true;
  }
  
  pushInput(filepath: string) {
    if (!this.files[filepath]) {
      this.files[filepath] = {
        added: true,
        updated: false,
        deleted: false,

        pushed: false,
        pushedPath: null
      };
    }

    this.files[filepath].updated = true;
  }

  // return value indicates if value is new
  pushOutput(filepath: string, pushedPath: string) {
    // if (!this.files[filepath]) this.pushInput(filepath);

    let item = this.files[filepath];

    item.pushed = true;
    item.pushedPath = pushedPath;

    return item.added;
  }

  retreiveDeleted() {
    return Object.keys(this.files)
      .filter((filepath) => {
        let item = this.files[filepath];
          
        return !item.added && item.updated && !item.pushed || item.deleted;
      })
      .map((filepath) => this.files[filepath].pushedPath as string);
  }


  commit() {
    for (let filepath in this.files) {
      let item = this.files[filepath];

      if (item.updated && !item.pushed || item.deleted) {
        delete this.files[filepath];
      }

      item.added = false;
      item.updated = false;
      item.pushed = false;
    }
  }

  discard() {
    for (let filepath in this.files) {
      let item = this.files[filepath];

      if (item.added) {
        delete this.files[filepath];
      }

      item.updated = false;
      item.pushed = false;
    }

    this.commit();
  }
}


export class SafeFileStore implements FileStore {
  private files: Set<string> = new Set();
  private preFiles: Set<string> = new Set();

  deleteInput(filepath: string) {}
  pushInput(filepath: string) {}

  pushOutput(filepath: string) {
    this.files.add(filepath);

    return !this.preFiles.has(filepath);
  }

  retreiveDeleted() {
    return [];
  }


  commit() {
    this.preFiles = this.files;
    this.discard();
  }

  discard() {
    this.files = new Set();
  }
}


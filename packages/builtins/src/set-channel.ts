/*!
 * mopane-builtins / set channel plugin
 * Licensed under the EUPL V.1.2
 */


import { ChannelSchema, FocusedPluginHandler, Interval } from 'mopane-core';


export function setChannel(sourceInput: Interval | number, target: number, options: SetChannelPluginOptions = {}): FocusedPluginHandler<ChannelSchema.Event> {
  let source = Interval.from(sourceInput);

  return function (emit) {
    let closedSources = 0;
    let isTargetClosed = false;

    return (event) => {
      switch (event.type) {
        case 'ch.active': {
          emit({
            type: 'ch.active',
            channels: (options.copy
              ? event.channels
              : event.channels.intersect(source.negate())
            ).unite(Interval.create(target))
          });

          break;
        }

        case 'ch.close': {
          if (event.channel === target) {
            isTargetClosed = true;
          } else if (!source.matchesValue(event.channel) || options.copy) {
            emit(event);
          }
            
          if (source.matchesValue(event.channel)) {
            closedSources++;
          }

          if (isTargetClosed && closedSources === source.size) {
            emit({ type: 'ch.close', channel: target });
          }

          break;
        }

        default: {
          if (event.channel !== void 0 && source.matchesValue(event.channel)) {
            emit({ ...event, channel: target });

            if (!options.copy) {
              return;
            }
          }

          emit(event);
        }
      }

    };
  };
}

export interface SetChannelPluginOptions {
  copy?: boolean;
}


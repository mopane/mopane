/*!
 * mopane-core / Lada
 * Licensed under the EUPL V.1.2
 */


import { PossiblyDeferred } from './util';


export class Lada<T> {
  endListeners: (() => void)[] = [];
  pushListeners: ((deferred: Promise<T>) => void)[] = [];
  isEnded: boolean = false;
  values: Promise<T>[] = [];

  constructor(values: PossiblyDeferred<T>[] = []) {
    for (let deferred of values) {
      this.push(deferred);
    }
  }

  add(value: PossiblyDeferred<T>): Lada<T> {
    let output = new Lada<T>(this.values.concat(Promise.resolve(value)));

    this.onEnd(() => {
      output.end();
    });

    return output;
  }

  concat<V = T>(other: Lada<V>): Lada<T | V> {
    let output = new Lada<T | V>();

    this.onPush((deferred) => {
      output.push(deferred);
    });

    other.onPush((deferred) => {
      output.push(deferred);
    });

    Promise.all([
      this.onEnd(),
      other.onEnd()
    ])
      .then(() => {
        output.end();
      });

    return output;
  }

  async copy(): Promise<Lada<T>> {
    return new Lada<T>(await this.toArray());
  }

  each(iteratee: (value: T) => void): Promise<void> {
    let promises: Promise<void>[] = [];

    this.onPush((deferred: Promise<T>) => {
      deferred.then(iteratee); // missing .catch(() => {})
      promises.push(deferred.then(() => {}));
    });

    return this.onEnd()
      .then(() => Promise.all(promises))
      .then(() => {});
  }

  end() {
    if (this.isEnded) {
      throw new Error('Illegal operation');
    }

    this.isEnded = true;

    for (let listener of this.endListeners) {
      listener();
    }
  }

  map<V = T>(iteratee: (value: T) => PossiblyDeferred<V>): Lada<V> {
    let output = new Lada<V>();

    this.onPush((deferred: Promise<T>) => {
      output.push(deferred.then(iteratee));
    });

    this.onEnd(() => {
      output.end();
    });

    return output;
  }

  mapMany<V = T>(iteratee: (value: T) => PossiblyDeferred<PossiblyDeferred<V>[]>): Lada<V> {
    let output = new Lada<V>();
    let promises: Promise<void>[] = [];

    this.onPush((sourceDeferred: Promise<T>) => {
      let promise = sourceDeferred
        .then(iteratee)
        .then((arr) => {
          for (let deferred of arr) {
            output.push(deferred);
          }
        });

      promises.push(promise);
    });

    this.onEnd()
      .then(() => Promise.all(promises))
      .then(() => { output.end(); });

    return output;
  }

  onEnd(): Promise<void>;
  onEnd(listener: () => void): void;
  onEnd(listener?: () => void): Promise<void> | void {
    if (!listener) {
      return new Promise((resolve) => {
        this.onEnd(resolve);
      });
    }

    if (this.isEnded) {
      listener();
    } else {
      this.endListeners.push(listener);
    }
  }

  onError(listener: (err: Error) => void) {
    this.onPush((deferred) => {
      deferred.catch((err) => {
        listener(err);
      });
    });
  }

  onPush(listener: (deferred: Promise<T>) => void) {
    for (let deferred of this.values) {
      listener(deferred);
    }

    if (!this.isEnded) {
      this.pushListeners.push(listener);
    }
  }

  pipe(other: Lada<T>) {
    this.onPush((deferred) => {
      other.push(deferred);
    });

    this.onEnd(() => {
      other.end();
    });
  }

  push(value: PossiblyDeferred<T>): this {
    if (this.isEnded) {
      throw new Error('Illegal operation');
    }

    let deferred = Promise.resolve(value);

    this.values.push(deferred);

    for (let listener of this.pushListeners) {
      listener(deferred);
    }

    return this;
  }

  split(separator: (value: T) => boolean): [Lada<T>, Lada<T>];
  split(numberOfOutputs: number, separator: (value: T) => number): Lada<T>[];
  split(a: any, b?: any): Lada<T>[] {
    let separator = b ? b : (value: T) => a(value) ? 1 : 0;
    let numberOfOutputs = b ? a : 2;

    let outputs = new Array(numberOfOutputs).fill(42).map(() => new Lada<T>());

    this.each((value) => {
      let outputIndex = separator(value);

      if (outputIndex >= numberOfOutputs) {
        throw new Error('Illegal operation');
      }

      outputs[outputIndex].push(value);
    })
      .then(() => {
        for (let output of outputs) {
          output.end();
        }
      });

    return outputs;
  }

  toArray(): Promise<T[]> {
    return new Promise((resolve) => {
      this.onEnd(() => {
        resolve(Promise.all(this.values));
      });
    });
  }

  static createEmpty<T = never>(): Lada<T> {
    let lada = new Lada<T>();

    lada.end();

    return lada;
  }
}

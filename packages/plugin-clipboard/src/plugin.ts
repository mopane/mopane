/*!
 * mopane-plugin-clipboard / plugin
 * Licensed under the EUPL V.1.2
 */


import { read as readClipboard, readSync as readClipboardSync, write as writeClipboard } from 'clipboardy';
import { DataStream, FocusedPlugin } from 'mopane-core';
import { FSSchema } from 'mopane-fs';


export function copyFile(filepath: string, options: { channel?: number; intercept?: boolean; } = {}): FocusedPlugin<FSSchema.Event> {
  let data: DataStream | null = null;

  return {
    run(emit) {
      return (event) => {
        if ((event.type === 'fs.create' || event.type === 'fs.update')
          && event.filepath === filepath
          && (options.channel === void 0 || event.channel === options.channel)) {

          data = event.contents;

          if (options.intercept) {
            return;
          }
        }

        emit(event);
      };
    },

    async commit() {
      if (data !== null) {
        await writeClipboard((await data.getBuffer()).toString());
      }

      data = null;
    },

    discard() {
      data = null;
    }
  };
}


export function pasteFile(filepath: string, options: { channel?: number; watch?: boolean; watchDelay?: number; } = {}): FocusedPlugin<FSSchema.Event> {
  let previousValue: string | null = null;
  let currentValue: string | null = null;
  let currentTestedValue: string | null = null;

  let interval: ReturnType<typeof setInterval> | null = null;

  return {
    initialize(context) {
      if (options.watch) {
        interval = setInterval(() => {
          if (context.runner.running) {
            return;
          }

          let value = readClipboardSync();

          if (value !== currentValue && value !== currentTestedValue) {
            context.runner.triggerRun();
          }
        }, options.watchDelay || 500);
      }
    },

    deinitialize() {
      if (options.watch) {
        clearInterval(interval!);
      }
    },

    run(emit) {
      let deferred = readClipboard()
        .then((value) => {
          currentValue = value;
          currentTestedValue = value;

          if (previousValue === null) {
            emit({
              type: 'fs.create',
              channel: options.channel,

              filepath,
              contents: DataStream.fromString(currentValue),
            });
          } else if (currentValue !== previousValue) {
            emit({
              type: 'fs.update',
              channel: options.channel,

              filepath,
              contents: DataStream.fromString(currentValue)
            });
          }
        })
        .catch((error) => {
          emit({ type: 'error', error });
        });

      return (event) => {
        if (event.type === 'end') {
          deferred.then(() => {
            emit(event);
          });
        } else {
          emit(event);
        }
      };
    },

    commit() {
      previousValue = currentValue;
    },

    discard() {
      currentValue = previousValue;
    }
  };
}


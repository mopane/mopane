/*!
 * mopane-builtins / guard plugin
 * Licensed under the EUPL V.1.2
 */


import { Plugin, SchemaGuard, SchemaGuardInstance } from 'mopane-core';


export function guard(handler: SchemaGuard): Plugin {
  let instance: SchemaGuardInstance;
  let done = true;

  return (emit) => {
    if (done) {
      done = false;
      instance = handler();
    }

    return (event) => {
      try {
        instance(event);
      } catch (error) {
        emit({ type: 'error', error });
      }

      if (event.type === 'end') {
        done = true;
      }

      emit(event);
    };
  };
}


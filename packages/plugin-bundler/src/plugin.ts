/*!
 * mopane-plugin-bundler / plugin
 * Licensed under the EUPL V.1.2
 */


import * as detective from 'detective';
import * as path from 'path';
import { DataStream, FocusedPlugin, FocusedPluginEmitterFunction, FocusedPluginReceivedEvent, PipelineEvent, PluginContext, RootSchema, StrictPlugin } from 'mopane-core';
import { FSSchema, absolutizePath, relativizePath } from 'mopane-fs';


export class BundlerPlugin implements FocusedPlugin<FSSchema.Event> {
  items: { [name: string]: Item; } = {};
  storedItems: { [name: string]: StoredItem; } = {};

  root: string | null = null;


  initialize(context: PluginContext) {
    this.root = context.project.root;
  }

  run(emit: FocusedPluginEmitterFunction<FSSchema.Event>) {
    return (event: FocusedPluginReceivedEvent<FSSchema.Event>) => {
      let isEntry = event.channel === 0;
      let isContext = event.channel === 1;

      let deferred: Promise<any>[] = [];

      if (event.type !== 'end' && !isEntry && !isContext) return;

      switch (event.type) {
        case 'fs.create': {
          this.items[event.filepath] = {
            contents: event.contents.getBuffer(),
            dependencies: null,
            entry: isEntry,
            refreshed: false,
            status: ItemStatus.Created
          };

          break;
        }

        case 'fs.update': {
          let item = this.items[event.filepath];

          item.status = ItemStatus.Updated;
          item.contents = event.contents.getBuffer();
          item.dependencies = null;

          break;
        }

        case 'fs.delete': {
          this.items[event.filepath].status = ItemStatus.Deleted;

          if (isEntry) {
            emit(event);
          }

          break;
        }

        case 'end': {
          let deferred: Promise<any>[] = [];

          for (let filepath in this.items) {
            let item = this.items[filepath];

            if (!item.entry || item.status === ItemStatus.Deleted) continue;

            deferred.push(
              this.processItem(filepath)
                .then((result) => {
                  return this.generateBundle(filepath, result);
                })
                .then((result) => {
                  emit({
                    channel: 0,
                    type: item.status === ItemStatus.Created ? 'fs.create' : 'fs.update',

                    filepath,
                    contents: DataStream.fromString(result),
                    mode: 0
                  } as FSSchema.FileCreationEvent | FSSchema.FileUpdateEvent);
                })
            );
          }

          Promise.all(deferred)
            .catch((error) => {
              emit({ type: 'error', error });
            })
            .then(() => {

              emit(event);
            });
        }
      }
    };
  }


  async commit() {
    this.storedItems = {};

    for (let filepath in this.items) {
      let item = this.items[filepath];

      if (item.status === ItemStatus.Deleted) {
        delete this.items[filepath];
      } else {
        item.status = ItemStatus.Ignored;
        item.refreshed = false;
      }

      this.storedItems[filepath] = {
        contents: await item.contents,
        dependencies: item.dependencies === null ? null : { ...item.dependencies },
        entry: item.entry
      };
    }
  }

  discard() {
    this.items = {};

    for (let filepath in this.storedItems) {
      let storedItem = this.storedItems[filepath];

      this.items[filepath] = {
        contents: Promise.resolve(storedItem.contents),
        dependencies: storedItem.dependencies === null ? null : { ...storedItem.dependencies },
        entry: storedItem.entry,
        refreshed: false,
        status: ItemStatus.Ignored
      };
    }
  }


  async generateBundle(entryFilepath: string, itemsExtracts: { [filepath: string]: ItemDependenciesReversed; }): Promise<string> {
    let output = ';(() => {let tree = {';

    let itemsFilepaths = Object.keys(itemsExtracts);

    for (let index = 0; index < itemsFilepaths.length; index++) {
      let filepath = itemsFilepaths[index];
      let dependenciesReversed = itemsExtracts[filepath];

      output += `${index}: [function(require, module, exports) {`;
      output += (await this.items[filepath].contents).toString();
      output += '},{';

      for (let requestedPath in dependenciesReversed) {
        let targetFilepath = dependenciesReversed[requestedPath];
        output += `'${requestedPath}': ${itemsFilepaths.indexOf(targetFilepath)},`;
      }

      output += '}],';
    }

    output += '};';

    output += `
      let require = (index) => {
        if (require.cache[index]) return require.cache[index];

        let func = tree[index][0];
        if (!func) throw new error('missing module ' + path);

        let module = { exports: {} };
        require.cache[index] = module.exports;

        func((path) => {
          return require(tree[index][1][path]);
        }, module, module.exports);

        return module.exports;
      };

      require.cache = {};

      require(${itemsFilepaths.indexOf(entryFilepath)});
      `;

    output += '})();';

    return output;
  }


  async loadItemDependencies(filepath: string): Promise<ItemDependencies> {
    let item = this.items[filepath];

    if (!item) {
      throw new Error(`Missing ${filepath}`);
    }

    if (item.dependencies !== null) return item.dependencies;

    if (!item.dependencies) {
      item.dependencies = {};

      let contents = (await item.contents).toString();
      let requestedPaths = detective(contents);

      for (let requestedPath of requestedPaths) {
        let possiblePaths = this.resolveDependency(filepath, requestedPath);
        let depPath = possiblePaths.find((p) => p in this.items);

        if (!depPath) {
          throw new Error(`Missing ${requestedPath} requested from ${filepath}`);
        }

        if (!item.dependencies[depPath]) {
          item.dependencies[depPath] = [];
        }

        item.dependencies[depPath].push(requestedPath);
      }
    }

    return item.dependencies;
  }

  async processItem(filepath: string, tree: string[] = []): Promise<{ [filepath: string]: ItemDependenciesReversed; }> {
    if (tree.includes(filepath)) {
      return {};
    }

    let item = this.items[filepath];

    if (!item || item.status === ItemStatus.Deleted) {
      throw new Error(`Missing file ${filepath} requested from ${tree.join(' -> ')}`);
    }

    if (!item.refreshed && (item.status === ItemStatus.Created || item.status === ItemStatus.Updated || item.dependencies === null)) {
      item.refreshed = true;

      await this.loadItemDependencies(filepath);
    }


    let outDependencies = {
      [filepath]: this.reverseDependencies(item.dependencies as ItemDependencies)
    };

    for (let depPath of Object.keys(item.dependencies as ItemDependencies)) {
      Object.assign(outDependencies, await this.processItem(depPath, [...tree, filepath]));
    }

    return outDependencies;
  }

  resolveDependency(source: string, target: string): string[] {
    let root = this.root as string;

    return [
      relativizePath(path.join(absolutizePath(path.dirname(source), root), target), root),
      relativizePath(path.join(absolutizePath(path.dirname(source), root), target + '.js'), root)
    ];
  }


  cloneDependencies(dependencies: ItemDependencies): ItemDependencies {
    let output: ItemDependencies = {};

    for (let dependencyPath in dependencies) {
      output[dependencyPath] = Array.from(dependencies[dependencyPath]);
    }

    return output;
  }

  reverseDependencies(dependencies: ItemDependencies): ItemDependenciesReversed {
    let output: ItemDependenciesReversed = {};

    for (let dependencyPath in dependencies) {
      for (let requestedPath of dependencies[dependencyPath]) {
        output[requestedPath] = dependencyPath;
      }
    }

    return output;
  }
}


export function plugin() {
  return new BundlerPlugin();
}


type ItemDependencies = { [filepath: string]: string[]; };
type ItemDependenciesReversed = { [requestedFilepath: string]: string; };

interface Item {
  contents: Promise<Buffer>;
  dependencies: ItemDependencies | null;
  entry: boolean;
  refreshed: boolean;
  status: ItemStatus;
}

interface StoredItem {
  contents: Buffer;
  dependencies: ItemDependencies | null;
  entry: boolean;
}

enum ItemStatus {
  Created,
  Deleted,
  Ignored,
  Updated
}


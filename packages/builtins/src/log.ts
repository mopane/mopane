/*!
 * mopane-builtins / log plugin
 * Licensed under the EUPL V.1.2
 */


import * as readline from 'readline';
import { ChannelSchema, FocusedPluginHandler, PipelineEvent } from 'mopane-core';


export function log(options: { persistent?: boolean } = {}): FocusedPluginHandler<ChannelSchema.Event> {
  return function (emit) {
    let channels: {
      [channelId: number]: {
        events: PipelineEvent[];
        isClosed: boolean;
      };
    } = {};

    let isFixed = false;

    let writtenLines = 0;
    let write = (value: string) => {
      readline.clearLine(process.stdout, 0);
      readline.moveCursor(process.stdout, 0, 0);
      process.stdout.write(value + '\n');
      writtenLines++;
    };

    let clearDisplay = () => {
      readline.moveCursor(process.stdout, 0, -writtenLines);
      readline.clearScreenDown(process.stdout);

      writtenLines = 0;
    };

    let updateDisplay = () => {
      clearDisplay();

      for (let _channelId in channels) {
        let channelId = parseInt(_channelId);
        let channel = channels[channelId];


        if (channel.events.length > 0 || !channel.isClosed) {
          if (channelId >= 0) {
            write(`- channel ${channelId}`);
          } else {
            write('- no channel');
          }
        }

        for (let event of channel.events) {
          write('  - ' + (event.toString ? event.toString() : event.type === 'string' ? `[${event.type}]` : '[symbol]'));
        }

        if (!channel.isClosed) {
          write ('  - (loading)');
        }
      }

      if (!isFixed) {
        write('- (loading)');
      }
    };

    updateDisplay();

    return (event) => {
      emit(event);

      switch (event.type) {
        case 'ch.active': {
          if (!event.channels.isFinite()) {
            emit({ type: 'error', error: new Error('Interval of active channels is infinite') });
            return;
          }

          for (let channelId of event.channels.values) {
            if (channels[channelId]) {
              continue;
            }

            channels[channelId] = {
              events: [],
              isClosed: false
            };
          }

          isFixed = true;

          break;
        }

        case 'ch.close': {
          if (channels[event.channel]) {
            channels[event.channel].isClosed = true;
          }

          break;
        }

        case 'end': {
          isFixed = true;

          for (let _channelId in channels) {
            let channelId = parseInt(_channelId);
            channels[channelId].isClosed = true;
          }

          if (!options.persistent) {
            clearDisplay();
            return;
          }

          break;
        }

        default: {
          let channelId = event.channel !== void 0 ? event.channel : -1;

          if (!channels[channelId]) {
            channels[channelId] = {
              events: [],
              isClosed: false
            };
          }

          channels[channelId].events.push(event);
        }
      }

      updateDisplay();
    };
  };
}


/*!
 * mopane-driver-chokidar / driver
 * Licensed under the EUPL V.1.2
 */


import * as chokidar from 'chokidar';
import { EventEmitter } from 'events';
import { DriverTypes } from 'mopane-core';


let watchers: chokidar.FSWatcher[] = [];

export const driver: DriverTypes['fs-watcher'] = {
  deinitialize() {
    for (let watcher of watchers) {
      watcher.close();
    }
  },

  request(options: chokidar.WatchOptions = {}) {
    return {
      watch(target: string) {
        let watcher = chokidar.watch(target, options);

        watchers.push(watcher);


        let ee = new EventEmitter();

        watcher.on('ready', () => {
          ee.emit('ready');
        });

        watcher.on('add', (path, stats) => {
          ee.emit('add', path, stats);
        });

        watcher.on('change', (path, stats) => {
          ee.emit('change', path, stats);
        });

        watcher.on('unlink', (path) => {
          ee.emit('delete', path);
        });

        return ee;
      }
    };
  }
};


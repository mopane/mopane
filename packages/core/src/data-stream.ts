/*!
 * mopane-core / data stream utilities
 * Licensed under the EUPL V.1.2
 */


import { Readable as ReadableStream } from 'stream';


export class DataStream {
  data: Buffer | null;
  stream: ReadableStream | null;

  constructor(input: Buffer | ReadableStream) {
    if (input instanceof Buffer) {
      this.data = input;
      this.stream = null;
    } else {
      this.data = null;
      this.stream = input;
    }
  }

  async getBuffer(): Promise<Buffer> {
    if (this.data === null) {
      if (this.stream === null) {
        throw new Error('Missing data');
      }

      this.data = await collectStream(this.stream);
      this.stream = null;
    }

    return this.data;
  }

  async getStream(): Promise<ReadableStream> {
    return createStream(await this.getBuffer());
  }


  static fromBuffer(buffer: Buffer) {
    return new DataStream(buffer);
  }

  static fromString(str: string) {
    return DataStream.fromBuffer(Buffer.from(str));
  }

  static fromStream(stream: ReadableStream) {
    return new DataStream(stream);
  }
}


export async function collectStream(stream: ReadableStream): Promise<Buffer> {
  let chunks: Buffer[] = [];

  stream.on('data', (chunk) => {
    chunks.push(Buffer.from(chunk));
  });

  return new Promise<Buffer>((resolve) => {
    stream.on('end', () => {
      resolve(Buffer.concat(chunks));
    });
  });
}

export function createStream(input: Buffer | string): ReadableStream {
  let stream = new ReadableStream();

  stream._read = () => {};

  stream.push(Buffer.from(input));
  stream.push(null);

  return stream;
}


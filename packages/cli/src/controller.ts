/*!
 * mopane-cli / controller
 * Licensed under the EUPL V.1.2
 */


import { ReadLine as Interface, clearLine, createInterface, clearScreenDown, cursorTo, moveCursor } from 'readline';
// import { Readable as ReadableStream, Writable as WritableStream } from 'stream';


export class Controller {
  private _message: string = '';
  private _status: string | null = null;

  private _spinnerFrameIndex: number = -1;
  private _spinnerInterval: ReturnType<typeof setInterval> | null = null;

  private _promptListener: ((line: string) => void) | null = null;
  private _screensHeight: number = 0;

  readonly input: typeof process.stdin;
  readonly output: typeof process.stdout;

  intf: Interface;
  screens: ControllerScreen[] = [];

  constructor(input: typeof process.stdin, output: typeof process.stdout) {
    this.input = input;
    this.output = output;

    this.intf = createInterface({
      input: this.input,
      output: this.output
    });

    this.intf.setPrompt('› ');

    this.output.write('\n');
  }

  async prompt(): Promise<string> {
    if (!this._promptListener) {
      this.intf.prompt();
    } else {
      this.intf.off('line', this._promptListener);
    }

    let value = await new Promise<string>((resolve) => {
      this._promptListener = (line) => {
        resolve(line);
      };

      this.intf.once('line', this._promptListener);
    });

    this._promptListener = null;

    moveCursor(this.input, 0, -1);
    clearLine(this.output, 0);

    if (value.length < 1) {
      return await this.prompt();
    }

    return value;
  }

  /* async promptKeypress(): Promise<string> {

  } */

  clear() {
    moveCursor(this.output, 0, -this._screensHeight - 1);
    cursorTo(this.output, 0);
    clearScreenDown(this.output);
  }

  close() {
    this.spinning = false;

    this.clear();
    this.intf.pause();
  }

  createScreen(index: number = 0) {
    let screen = new ControllerScreen(this);
    this.screens.splice(index, 0, screen);

    return screen;
  }

  updateBar() {
    let initialCursorX = this.intf.cursor + 2;

    moveCursor(this.output, 0, -1);
    cursorTo(this.output, 0);
    clearLine(this.output, 0);

    this.writeBar();

    moveCursor(this.output, 0, 1);
    cursorTo(this.output, initialCursorX);
  }

  update() {
    this.clear();
    this._screensHeight = 0;

    for (let screen of this.screens) {
      this._screensHeight += screen.contents.split('\n').length - 1;

      this.output.write(screen.contents);
    }

    this.writeBar();

    this.output.write('\n› ' + this.intf.line);
  }

  writeBar() {
    let spinnerText = '';

    if (this._spinnerFrameIndex >= 0) {
      spinnerText = spinnerFrames[this._spinnerFrameIndex] + ' ';
      this.output.write(spinnerText);
    }

    this.output.write(this._message);

    if (this._status !== null) {
      this.output.write(' '.repeat(Math.min(this.output.columns || 80, 80) - spinnerText.length - this._message.length - this._status.length - 2));
      this.output.write(`[${this._status}]`);
    }
  }


  set message(message: string) {
    this._message = message;
    this.updateBar();
  }

  set status(status: string | null) {
    this._status = status;
    this.updateBar();
  }

  set spinning(value: boolean) {
    if (value && this._spinnerFrameIndex < 0) {
      this._spinnerFrameIndex = 0;
      this.updateBar();

      this._spinnerInterval = setInterval(() => {
        this._spinnerFrameIndex = (this._spinnerFrameIndex + 1) % spinnerFrames.length;
        this.updateBar();
      }, 80);
    }

    if (!value && this._spinnerFrameIndex >= 0) {
      clearInterval(this._spinnerInterval!);
      this._spinnerInterval = null;

      this._spinnerFrameIndex = -1;
      this.updateBar();
    }
  }
}

export class ControllerScreen {
  private _contents: string = '';

  constructor(readonly controller: Controller) {}

  remove() {
    this.controller.screens.splice(this.controller.screens.indexOf(this), 1);
    this.controller.update();
  }

  get contents(): string {
    return this._contents;
  }

  set contents(contents: string) {
    this._contents = contents;
    this.controller.update();
  }
}


const spinnerFrames = [
  '⠋', '⠙', '⠹', '⠸', '⠼', '⠴', '⠦', '⠧', '⠇', '⠏'
];


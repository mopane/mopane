/*!
 * mopane-fs / OutputManager
 * Licensed under the EUPL V.1.2
 */


import * as path from 'path';
import { Component, TaskRunner } from 'mopane-core';
import { promisify } from 'util';

import { FSSchema } from './schemas/fs';
import { absolutizePath, fsutil } from './util';


export class OutputManager implements Component {
  runner: TaskRunner;

  constructor(runner: TaskRunner) {
    this.runner = runner;
  }

  async initialize() {

  }

  async consume(event: FSSchema.Event, options: ConsumptionOptions = {}) {
    let fs = await this.runner.profile.requestDriver('fs');
    let targetPath = absolutizePath(event.filepath, this.runner.project.root);

    let fd;

    switch (event.type) {
      case 'fs.create':
        if (!options.disableStructure) {
          await fsutil.mkdirp(fs, path.dirname(targetPath));
        }

        let flags = options.disableOverride ? 'wx' : 'w';
        fd = await promisify(fs.open)(targetPath, flags);

        await fsutil.writeFromStream(fs, fd, await event.contents.getStream());

        break;

      case 'fs.update':
        fd = await promisify(fs.open)(targetPath, 'r+');

        await fsutil.writeFromStream(fs, fd, await event.contents.getStream());

        break;

      case 'fs.delete':
        await promisify(fs.unlink)(targetPath);

        break;

      /* default:
        throw new Error(`Unsupported event type '${event.type}'`); */
    }
  }

  async deinitialize() {

  }
}

interface ConsumptionOptions {
  disableOverride?: boolean;
  disableStructure?: boolean;
}


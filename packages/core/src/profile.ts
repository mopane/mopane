/*!
 * mopane-core / Profile
 * Licensed under the EUPL V.1.2
 */


import { DriverInstances, DriverName, DriverTypes } from './drivers';


export class Profile {
  drivers: Partial<DriverTypes> = {};

  async isDriverAvailable(name: DriverName): Promise<boolean> {
    let driver = this.drivers[name];

    return driver !== void 0 && (!driver.isAvailable || await driver.isAvailable());
  }

  async requestDriver<N extends DriverName>(name: N): Promise<DriverInstances[N]> {
    if (!this.drivers[name]) {
      throw new Error(`Missing driver '${name}'`);
    }

    return await (this.drivers[name] as DriverTypes[N]).request();
  }

  async registerDriver<N extends DriverName>(name: N, driver: DriverTypes[N]) {
    if (this.drivers[name]) {
      throw new Error(`Driver '${name}' is already registered`);
    }

    this.overrideDriver(name, driver);
  }

  async registerDrivers(drivers: Partial<DriverTypes>) {
    await Promise.all(
      (Object.keys(drivers) as DriverName[]).map(<N extends DriverName>(name: N) =>
        this.registerDriver(name, drivers[name] as DriverTypes[N])
      )
    );
  }

  async overrideDriver<N extends DriverName>(name: N, driver: DriverTypes[N]) {
    this.drivers[name] = driver;

    if (driver.initialize) await driver.initialize();
  }

  async unregisterDriver<N extends DriverName>(name: N) {
    let driver = this.drivers[name] as DriverTypes[N];

    if (driver.deinitialize) await driver.deinitialize();

    delete this.drivers[name];
  }

  async unregisterAllDrivers() {
    await Promise.all(
      (Object.keys(this.drivers) as DriverName[])
        .map((name) => this.unregisterDriver(name))
    );
  }
}


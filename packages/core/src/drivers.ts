/*!
 * mopane-core / drivers
 * Licensed under the EUPL V.1.2
 */


import * as fs from 'fs';
import * as http from 'http';
import * as https from 'https';
import { Stats } from 'fs';

import { Component } from './component';
import { PossiblyDeferred } from './util';


export interface Driver<Instance extends DriverInstance = DriverInstance> extends Partial<Component> {
  isAvailable?(): PossiblyDeferred<boolean>;
  request(): PossiblyDeferred<Instance>;
}

export type DriverInstance = object;


export interface DriverInstances {
  fs: typeof fs;
  "fs-watcher": DriverInstanceFSWatcher;

  http: typeof http;
  https: typeof https;
}

export type DriverName = keyof DriverInstances;

export type DriverTypes = {
  [N in DriverName]: Driver<DriverInstances[N]>;
};


interface DriverInstanceFSWatcher {
  watch(path: string): {
    on(event: 'add' | 'change', listener: (path: string, stats?: Stats) => void): void;
    on(event: 'delete', listener: (path: string) => void): void;
    on(event: 'error', listener: (err: Error) => void): void;
    on(event: 'ready', listener: () => void): void;
  };
}


export namespace BuiltinDrivers {
  export const fs: DriverTypes['fs'] = {
    async request() {
      try {
        // The 'graceful-fs' module is often present in node_modules because
        // of other dependencies, and is preferred to default 'fs'.
        return await import('graceful-fs');
      } catch (err) {

      }

      return await import('fs');
    }
  };

  export const http: DriverTypes['http'] = {
    request: async () => await import('http')
  };

  export const https: DriverTypes['https'] = {
    request: async () => await import('https')
  };
}


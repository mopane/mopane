/*!
 * mopane-builtins / read plugin
 * Licensed under the EUPL V.1.2
 */


import { FocusedPlugin, FocusedPluginEmitterFunction, FocusedPluginReceivedEvent, PluginContext } from 'mopane-core';
import { Descriptor, FSSchema, absolutizePath } from 'mopane-fs';


export function read(selector: string, options: Options = {}) {
  return new ReadPlugin(selector, options);
}

export class ReadPlugin implements FocusedPlugin<never, FSSchema.Event> {
  context: PluginContext | undefined;
  deferredDescriptor: Promise<Descriptor> | undefined;
  options: Options;
  selector: string;

  isRunning: boolean = false;

  constructor(selector: string, options: Options) {
    this.options = options;
    this.selector = selector;
  }

  async initialize(context: PluginContext) {
    this.context = context;

    let target = absolutizePath(this.selector, this.context.project.root);

    this.deferredDescriptor = this.context.runner.fsInputInterface.requestFile(target);

    if (!this.options.lazy) {
      await this.deferredDescriptor;
    } else {
      // Make sure no 'unhandled promise rejection' errors arise.
      this.deferredDescriptor.catch(() => {});
    }
  }

  run(emit: FocusedPluginEmitterFunction<FSSchema.Event>) {
    this.isRunning = true;

    let deferredEmitting = (this.deferredDescriptor as Promise<Descriptor>)
      .then((desc) => {
        let events = (this.context as PluginContext).runner.fsInputInterface.collectEvents(desc);

        for (let event of events) {
          emit({ ...event, channel: this.options.channel });
        }
      })
      .catch((err) => {
        emit({ type: 'error', error: err });
      });

    return (event: FocusedPluginReceivedEvent<never>) => {
      if (event.type !== 'end') {
        emit(event);
        return;
      }

      deferredEmitting.then(() => {
        emit(event);
        this.isRunning = false;
      });
    };
  }

  async deinitialize() {
    (this.deferredDescriptor as Promise<Descriptor>)
      // Errors here have been handled above.
      .catch(() => -1)
      .then((desc) => {
        if (desc < 0) {
          return;
        }

        (this.context as PluginContext).runner.fsInputInterface.releaseDescriptor(desc);
      });
  }
}

interface Options {
  channel?: number;
  lazy?: boolean;
}


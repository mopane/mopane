/*!
 * mopane-builtins / tree plugin
 * Licensed under the EUPL V.1.2
 */


import * as path from 'path';
import * as readline from 'readline';
import { FocusedPlugin } from 'mopane-core';
import { FSSchema } from 'mopane-fs';

import { TreeDataLike, formatTree } from './util';


export function tree(options: { persistent?: boolean; } = {}): FocusedPlugin<FSSchema.Event> {
  let writtenLines = 0;
  let write = (value: string) => {
    readline.clearLine(process.stdout, 0);
    readline.moveCursor(process.stdout, 0, 0);
    process.stdout.write(value + '\n');
    writtenLines++;
  };


  let preTree: TreeDataLike = {};
  let tree: TreeDataLike = {};

  let updateDisplay = () => {
    clearDisplay();
    write('.');

    for (let line of formatTree(tree)) {
      write(line);
    }
  };

  let clearDisplay = () => {
    readline.moveCursor(process.stdout, 0, -writtenLines);
    readline.clearScreenDown(process.stdout);

    writtenLines = 0;
  };

  return {
    run(emit) {
      let writeItem = (segments: string[], subtree: TreeDataLike, head: string) => {
        if (segments.length < 1) {
          subtree[head] = {};
          return;
        }

        let [seg0, ...others] = segments;

        if (!subtree[seg0]) {
          subtree[seg0] = {};
        }

        writeItem(others, subtree[seg0], head);
      };

      let findItem = (segments: string[], subtree: TreeDataLike): TreeDataLike => {
        if (segments.length < 1) {
          return subtree;
        }

        let [seg0, ...others] = segments;

        return findItem(others, subtree[seg0]);
      };

      let removeItem = (segments: string[], subtree: TreeDataLike, head: string) => {
        let parent = findItem(segments, subtree);

        delete parent[head];

        if (segments.length < 1) {
          return;
        }

        if (Object.keys(parent).length < 1) {
          let newHead = segments.pop() as string;
          removeItem(segments, subtree, newHead);
        }
      };

      return (event) => {
        emit(event);

        if (event.type !== 'fs.create' && event.type !== 'fs.delete') {
          return;
        }

        let segments = [event.channel !== void 0 ? `channel ${event.channel}` : 'no channel']
          .concat(path.dirname(event.filepath).split('/'));
        let head = path.basename(event.filepath);

        switch (event.type) {
          case 'fs.create':
            writeItem(segments, tree, head);
            break;

          case 'fs.delete':
            removeItem(segments, tree, head);
            break;
        }

        updateDisplay();
      };
    },

    deinitialize() {
      if (!options.persistent) {
        clearDisplay();
      }
    },

    commit() {
      preTree = Object.assign({}, tree);
    },

    discard() {
      tree = Object.assign({}, preTree);
      updateDisplay();
    }
  };
}


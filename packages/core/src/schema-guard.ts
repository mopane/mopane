/*!
 * mopane-core / schema guard
 * Licensed under the EUPL V.1.2
 */


import { PipelineEvent } from './plugin';


export type SchemaGuard = () => SchemaGuardInstance;
export type SchemaGuardInstance = (event: PipelineEvent) => void;


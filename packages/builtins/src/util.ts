/*!
 * mopane-builtins / utilities
 * Licensed under the EUPL V.1.2
 */


export async function arrEveryAsync<T>(
  arr: T[],
  callback: (value: T, index: number) => PossiblyDeferred<boolean | void>
): Promise<boolean> {
  for (let index = 0; index < arr.length; index++) {
    let value = arr[index];

    if (!(await callback(value, index))) {
      return false;
    }
  }

  return true;
}

export async function arrFilterAsync<T>(
  arr: T[],
  callback: (value: T, index: number) => PossiblyDeferred<boolean | void>
): Promise<T[]> {
  return (await Promise.all(
    arr.map(async (value, index): Promise<[T, boolean]> =>
      [value, Boolean(await callback(value, index))]
    )
  ))
    .filter(([value, keep]) => keep)
    .map(([value, keep]) => value);
}

export async function arrSomeAsync<T>(
  arr: T[],
  callback: (value: T, index: number) => PossiblyDeferred<boolean | void>
): Promise<boolean> {
  for (let index = 0; index < arr.length; index++) {
    let value = arr[index];

    if (await callback(value, index)) {
      return true;
    }
  }

  return false;
}


export function formatTree(tree: TreeData | TreeDataLike, bars: boolean[] = []) {
  let treeLikeToTree = (obj: TreeDataLike): TreeData => {
    return Object.keys(obj).reduce((tree, name) => tree.set(name, obj[name]), new Map());
  };

  if (!(tree instanceof Map)) {
    tree = treeLikeToTree(tree);
  }

  let lines: string[] = [];
  let index = 0;

  for (let [name, children] of tree) {
    let isLast = ++index >= tree.size;

    lines.push(
      bars.map((bar) => (bar ? '│   ' : '    ')).join('')
    + (isLast ? '└── ' : '├── ')
    + name
    );

    lines = lines.concat(
      formatTree(children, bars.concat(!isLast))
    );
  }

  return lines;
}


// interfaces must be used to allow recursion
export interface TreeData extends Map<string, TreeData> {}
export interface TreeDataLike { [name: string]: TreeDataLike; }

export type PossiblyDeferred<T> = Promise<T> | T;

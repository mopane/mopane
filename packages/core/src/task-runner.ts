/*!
 * mopane-core / TaskRunner
 * Licensed under the EUPL V.1.2
 */


import { EventEmitter } from 'events';
import { InputManager, OutputManager } from 'mopane-fs';

import { Component, RunnableComponent } from './component';
import { Processor } from './processor';
import { Task, TaskBuilderContext } from './task-builder';
import { PluginListenerFunction } from './plugin';
import { Project } from './project';
import { Profile } from './profile';
import { RootSchema } from './schemas/root';
import { PossiblyDeferred } from './util';


export class TaskRunner extends EventEmitter implements RunnableComponent {
  processor: Processor | null = null;
  project: Project;
  profile: Profile;
  task: Task;

  fsInputInterface: InputManager;
  fsOutputInterface: OutputManager;

  layers: Partial<TaskRunnerLayer>[] = [];
  layerMethods: {
    deinitialize: TaskRunnerLayer['deinitialize'][],
    initialize: TaskRunnerLayer['initialize'][],
    postRun: TaskRunnerLayer['postRun'][]
    preRun: TaskRunnerLayer['preRun'][],
  } = {
    deinitialize: [],
    initialize: [],
    postRun: [],
    preRun: [],
  };

  aborted: boolean = false;
  running: boolean = false;
  runTriggered: boolean = false;

  persistent: boolean = false;

  constructor(task: Task, project: Project, options: TaskRunnerOptions = {}) {
    super();

    this.project = project;
    this.task = task;
    this.profile = new Profile();

    this.persistent = options.persistent || false;

    this.fsInputInterface = new InputManager(this);
    this.fsOutputInterface = new OutputManager(this);
  }

  abort() {
    if (!this.running) {
      throw new Error('Invalid call');
    }

    this.aborted = true;
  }

  async deinitialize() {
    await Promise.all([
      (this.processor as Processor).deinitialize(),
      this.fsInputInterface.deinitialize(),
      this.fsOutputInterface.deinitialize(),
      this.profile.unregisterAllDrivers(),
      ...this.layerMethods.deinitialize.map((method) => method())
    ]);
  }

  async initialize() {
    let taskContext: TaskBuilderContext = {
      project: this.project,
      runner: this
    };

    let plugins = await this.task.prepare(taskContext, {});
    this.processor = new Processor(plugins, { runner: this });

    await Promise.all([
      this.fsInputInterface.initialize(),
      this.fsOutputInterface.initialize()
    ]);

    await Promise.all([
      this.processor.initialize(),
      ...this.layerMethods.initialize.map((method) => method())
    ]);
  }

  async run() {
    this.running = true;
    this.runTriggered = false;

    let runErr: Error | null = null;
    let commitErr: Error | null = null;

    let processor = this.processor as Processor;

    try {

      // run TaskRunnerLayer#preRun()
      await Promise.all(this.layerMethods.preRun.map((method) => method()));

      await new Promise((resolve, reject) => {
        let error: Error | null = null;

        let emitter: PluginListenerFunction = (event) => {
          switch (event.type) {
            case 'end':
              if (error) {
                reject(error);
              } else {
                resolve();
              }

              break;

            case 'error':
              this.aborted = true;

              if (!error) {
                error = (event as RootSchema.ErrorEvent).error;
              }

              break;
          }
        };

        let endEvent = { type: 'end' };
        let listener = processor.run(emitter);

        listener(endEvent);
      });

      // run TaskRunnerLayer#postRun()
      await Promise.all(this.layerMethods.postRun.map((method) => method()));

    } catch (_runErr) {
      runErr = _runErr;
    }

    try {
      if (runErr) {
        await processor.discard();
      } else {
        await processor.commit();
      }
    } catch (_commitErr) {
      commitErr = _commitErr;
    }

    // It is important that 'running' is still set to true while endRun() is
    // executed, otherwise a new run would immediately start if the input
    // manager requests it, without waiting for the current method to end.
    this.fsInputInterface.endRun(this.aborted);

    this.aborted = false;
    this.running = false;

    if (this.runTriggered) {
      this.triggerRun();
    }

    if (runErr) throw runErr;
    if (commitErr) throw commitErr;
  }

  use(layer: Partial<TaskRunnerLayer>, options: { place?: 'inside' | 'outside' } = {}) {
    this.layers.push(layer);

    let isInside = options.place === 'inside'; // so default (void) is outside
    let preMethod: 'push' | 'unshift' = isInside ? 'push': 'unshift';
    let postMethod: 'push' | 'unshift' = isInside ? 'unshift' : 'push';

    // ordered in chronological order
    if (layer.initialize) {
      this.layerMethods.initialize.push(layer.initialize.bind(layer));
    } if (layer.preRun) {
      this.layerMethods.preRun[preMethod](layer.preRun.bind(layer));
    } if (layer.postRun) {
      this.layerMethods.postRun[postMethod](layer.postRun.bind(layer));
    } if (layer.deinitialize) {
      this.layerMethods.deinitialize.push(layer.deinitialize.bind(layer));
    }
  }

  triggerRun() {
    if (this.running) {
      this.runTriggered = true;
    } else {
      this.emit('trigger');
    }
  }
}

export interface TaskRunnerOptions {
  persistent?: boolean;
}

export interface TaskRunnerLayer extends Component {
  deinitialize(): PossiblyDeferred<void>;
  initialize(): PossiblyDeferred<void>;

  preRun(): PossiblyDeferred<void>;
  postRun(): PossiblyDeferred<void>;
}

